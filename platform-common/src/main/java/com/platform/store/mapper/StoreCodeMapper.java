package com.platform.store.mapper;

import com.platform.store.domain.StoreCode;
import java.util.List;

/**
 * 手机验证码Mapper接口
 * 
 * @author platform
 * @date 2020-03-04
 */
public interface StoreCodeMapper 
{
    /**
     * 查询手机验证码
     * 
     * @param codeId 手机验证码ID
     * @return 手机验证码
     */
    public StoreCode selectStoreCodeById(Integer codeId);

    /**
     * 查询手机验证码列表
     * 
     * @param storeCode 手机验证码
     * @return 手机验证码集合
     */
    public List<StoreCode> selectStoreCodeList(StoreCode storeCode);

    /**
     * 新增手机验证码
     * 
     * @param storeCode 手机验证码
     * @return 结果
     */
    public int insertStoreCode(StoreCode storeCode);

    /**
     * 修改手机验证码
     * 
     * @param storeCode 手机验证码
     * @return 结果
     */
    public int updateStoreCode(StoreCode storeCode);

    /**
     * 删除手机验证码
     * 
     * @param codeId 手机验证码ID
     * @return 结果
     */
    public int deleteStoreCodeById(Integer codeId);

    /**
     * 批量删除手机验证码
     * 
     * @param codeIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteStoreCodeByIds(String[] codeIds);
}
