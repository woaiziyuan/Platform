package com.platform.store.mapper;

import com.platform.store.domain.StoreBlacklist;
import java.util.List;

/**
 * 黑名单Mapper接口
 * 
 * @author platform
 * @date 2020-02-26
 */
public interface StoreBlacklistMapper 
{
    /**
     * 查询黑名单
     * 
     * @param id 黑名单ID
     * @return 黑名单
     */
    public StoreBlacklist selectStoreBlacklistById(Integer id);

    /**
     * 查询黑名单列表
     * 
     * @param storeBlacklist 黑名单
     * @return 黑名单集合
     */
    public List<StoreBlacklist> selectStoreBlacklistList(StoreBlacklist storeBlacklist);

    /**
     * 新增黑名单
     * 
     * @param storeBlacklist 黑名单
     * @return 结果
     */
    public int insertStoreBlacklist(StoreBlacklist storeBlacklist);

    /**
     * 修改黑名单
     * 
     * @param storeBlacklist 黑名单
     * @return 结果
     */
    public int updateStoreBlacklist(StoreBlacklist storeBlacklist);

    /**
     * 删除黑名单
     * 
     * @param id 黑名单ID
     * @return 结果
     */
    public int deleteStoreBlacklistById(Integer id);

    /**
     * 批量删除黑名单
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteStoreBlacklistByIds(String[] ids);
}
