package com.platform.store.mapper;

import com.platform.store.domain.StoreChannle;
import java.util.List;

/**
 * 渠道信息Mapper接口
 * 
 * @author platform
 * @date 2020-03-03
 */
public interface StoreChannleMapper 
{
    /**
     * 查询渠道信息
     * 
     * @param id 渠道信息ID
     * @return 渠道信息
     */
    public StoreChannle selectStoreChannleById(Long id);

    /**
     * 查询渠道信息列表
     * 
     * @param storeChannle 渠道信息
     * @return 渠道信息集合
     */
    public List<StoreChannle> selectStoreChannleList(StoreChannle storeChannle);

    /**
     * 新增渠道信息
     * 
     * @param storeChannle 渠道信息
     * @return 结果
     */
    public int insertStoreChannle(StoreChannle storeChannle);

    /**
     * 修改渠道信息
     * 
     * @param storeChannle 渠道信息
     * @return 结果
     */
    public int updateStoreChannle(StoreChannle storeChannle);

    /**
     * 删除渠道信息
     * 
     * @param id 渠道信息ID
     * @return 结果
     */
    public int deleteStoreChannleById(Long id);

    /**
     * 批量删除渠道信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteStoreChannleByIds(String[] ids);
}
