package com.platform.store.mapper;

import com.platform.store.domain.StoreCoupon;
import java.util.List;

/**
 * 券码Mapper接口
 * 
 * @author platform
 * @date 2020-03-04
 */
public interface StoreCouponMapper 
{
    /**
     * 查询券码
     * 
     * @param id 券码ID
     * @return 券码
     */
    public StoreCoupon selectStoreCouponById(Long id);

    /**
     * 查询券码列表
     * 
     * @param storeCoupon 券码
     * @return 券码集合
     */
    public List<StoreCoupon> selectStoreCouponList(StoreCoupon storeCoupon);

    /**
     * 新增券码
     * 
     * @param storeCoupon 券码
     * @return 结果
     */
    public int insertStoreCoupon(StoreCoupon storeCoupon);

    /**
     * 修改券码
     * 
     * @param storeCoupon 券码
     * @return 结果
     */
    public int updateStoreCoupon(StoreCoupon storeCoupon);

    /**
     * 删除券码
     * 
     * @param id 券码ID
     * @return 结果
     */
    public int deleteStoreCouponById(Long id);

    /**
     * 批量删除券码
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteStoreCouponByIds(String[] ids);
}
