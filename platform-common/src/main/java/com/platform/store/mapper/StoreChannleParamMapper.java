package com.platform.store.mapper;

import com.platform.store.domain.StoreChannleParam;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 渠道网关参数Mapper接口
 * 
 * @author platform
 * @date 2020-03-05
 */
public interface StoreChannleParamMapper 
{
    /**
     * 查询渠道网关参数
     * 
     * @param id 渠道网关参数ID
     * @return 渠道网关参数
     */
    public StoreChannleParam selectStoreChannleParamById(Integer id);

    /**
     * 查询渠道网关参数列表
     * 
     * @param storeChannleParam 渠道网关参数
     * @return 渠道网关参数集合
     */
    public List<StoreChannleParam> selectStoreChannleParamList(StoreChannleParam storeChannleParam);

    /**
     * 新增渠道网关参数
     * 
     * @param storeChannleParam 渠道网关参数
     * @return 结果
     */
    public int insertStoreChannleParam(StoreChannleParam storeChannleParam);

    /**
     * 修改渠道网关参数
     * 
     * @param storeChannleParam 渠道网关参数
     * @return 结果
     */
    public int updateStoreChannleParam(StoreChannleParam storeChannleParam);

    /**
     * 删除渠道网关参数
     * 
     * @param id 渠道网关参数ID
     * @return 结果
     */
    public int deleteStoreChannleParamById(Integer id);

    /**
     * 批量删除渠道网关参数
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteStoreChannleParamByIds(String[] ids);

    /**
     * 批量删除渠道网关参数
     *
     * @param channleId 渠道ID
     * @param gateId 网关
     * @return 结果
     */
    List<StoreChannleParam> selectByChannleIdAndGateId(@Param("channleId") Integer channleId, @Param("gateId") String gateId);
}
