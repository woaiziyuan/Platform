package com.platform.store.domain;

import com.platform.common.annotation.Excel;
import com.platform.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 渠道网关参数对象 store_channle_param
 * 
 * @author platform
 * @date 2020-03-05
 */
public class StoreChannleParam extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 表id */
    private Integer id;

    /** 手机号 */
    @Excel(name = "手机号")
    private Long channleId;

    /** 网关 */
    @Excel(name = "网关")
    private String gateId;

    /** 参数名 */
    @Excel(name = "参数名")
    private String paraKey;

    /** 参数值 */
    @Excel(name = "参数值")
    private String paraValue;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setId(Integer id) 
    {
        this.id = id;
    }

    public Integer getId() 
    {
        return id;
    }
    public void setChannleId(Long channleId) 
    {
        this.channleId = channleId;
    }

    public Long getChannleId() 
    {
        return channleId;
    }
    public void setGateId(String gateId) 
    {
        this.gateId = gateId;
    }

    public String getGateId() 
    {
        return gateId;
    }
    public void setParaKey(String paraKey) 
    {
        this.paraKey = paraKey;
    }

    public String getParaKey() 
    {
        return paraKey;
    }
    public void setParaValue(String paraValue) 
    {
        this.paraValue = paraValue;
    }

    public String getParaValue() 
    {
        return paraValue;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("channleId", getChannleId())
            .append("gateId", getGateId())
            .append("paraKey", getParaKey())
            .append("paraValue", getParaValue())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
