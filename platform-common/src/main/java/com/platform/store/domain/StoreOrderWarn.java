package com.platform.store.domain;

import com.platform.common.annotation.Excel;
import com.platform.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 订单预警对象 store_order_warn
 * 
 * @author platform
 * @date 2020-02-25
 */
public class StoreOrderWarn extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String ids;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long version;

    /** 用户表外键 */
    @Excel(name = "用户表外键")
    private String userid;

    /** 子订单编号 */
    @Excel(name = "子订单编号")
    private String storeordernum;

    /** 是否处理（1-券码不足；2-修改订单状态失败；3-处理成功） */
    @Excel(name = "是否处理", readConverterExp = "1=-券码不足；2-修改订单状态失败；3-处理成功")
    private Integer state;

    /** 手机号 */
    @Excel(name = "手机号")
    private String telphone;

    /** 创建时间 */
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdate;

    public void setIds(String ids) 
    {
        this.ids = ids;
    }

    public String getIds() 
    {
        return ids;
    }
    public void setVersion(Long version) 
    {
        this.version = version;
    }

    public Long getVersion() 
    {
        return version;
    }
    public void setUserid(String userid) 
    {
        this.userid = userid;
    }

    public String getUserid() 
    {
        return userid;
    }
    public void setStoreordernum(String storeordernum) 
    {
        this.storeordernum = storeordernum;
    }

    public String getStoreordernum() 
    {
        return storeordernum;
    }
    public void setState(Integer state) 
    {
        this.state = state;
    }

    public Integer getState() 
    {
        return state;
    }
    public void setTelphone(String telphone) 
    {
        this.telphone = telphone;
    }

    public String getTelphone() 
    {
        return telphone;
    }
    public void setCreatedate(Date createdate) 
    {
        this.createdate = createdate;
    }

    public Date getCreatedate() 
    {
        return createdate;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("ids", getIds())
            .append("version", getVersion())
            .append("userid", getUserid())
            .append("storeordernum", getStoreordernum())
            .append("state", getState())
            .append("telphone", getTelphone())
            .append("remark", getRemark())
            .append("createdate", getCreatedate())
            .toString();
    }
}
