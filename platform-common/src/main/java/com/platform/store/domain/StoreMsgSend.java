package com.platform.store.domain;

import com.platform.common.annotation.Excel;
import com.platform.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 信息发送对象 store_msg_send
 * 
 * @author platform
 * @date 2020-03-04
 */
public class StoreMsgSend extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private Long msgId;

    /** 系统订单号 */
    @Excel(name = "系统订单号")
    private Integer orderId;

    /** 订单编号 */
    @Excel(name = "订单编号")
    private String orderSn;

    /** 券码id */
    @Excel(name = "券码id")
    private Long couponId;

    /** 收货人手机号 */
    @Excel(name = "收货人手机号")
    private String phone;

    /** 内容 */
    @Excel(name = "内容")
    private String content;

    /** 发送状态 */
    @Excel(name = "发送状态")
    private Integer status;

    /** 短信发送时间 */
    @Excel(name = "短信发送时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date sendTime;

    /** 短信接口返回码 */
    @Excel(name = "短信接口返回码")
    private String resCode;

    /** 短信接口返回信息 */
    @Excel(name = "短信接口返回信息")
    private String resMsg;

    /** 重试次数 */
    @Excel(name = "重试次数")
    private Integer repeatCount;

    public void setMsgId(Long msgId) 
    {
        this.msgId = msgId;
    }

    public Long getMsgId() 
    {
        return msgId;
    }
    public void setOrderId(Integer orderId) 
    {
        this.orderId = orderId;
    }

    public Integer getOrderId() 
    {
        return orderId;
    }
    public void setOrderSn(String orderSn) 
    {
        this.orderSn = orderSn;
    }

    public String getOrderSn() 
    {
        return orderSn;
    }
    public void setCouponId(Long couponId) 
    {
        this.couponId = couponId;
    }

    public Long getCouponId() 
    {
        return couponId;
    }
    public void setPhone(String phone) 
    {
        this.phone = phone;
    }

    public String getPhone() 
    {
        return phone;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setSendTime(Date sendTime) 
    {
        this.sendTime = sendTime;
    }

    public Date getSendTime() 
    {
        return sendTime;
    }
    public void setResCode(String resCode) 
    {
        this.resCode = resCode;
    }

    public String getResCode() 
    {
        return resCode;
    }
    public void setResMsg(String resMsg) 
    {
        this.resMsg = resMsg;
    }

    public String getResMsg() 
    {
        return resMsg;
    }
    public void setRepeatCount(Integer repeatCount) 
    {
        this.repeatCount = repeatCount;
    }

    public Integer getRepeatCount() 
    {
        return repeatCount;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("msgId", getMsgId())
            .append("orderId", getOrderId())
            .append("orderSn", getOrderSn())
            .append("couponId", getCouponId())
            .append("phone", getPhone())
            .append("content", getContent())
            .append("status", getStatus())
            .append("sendTime", getSendTime())
            .append("resCode", getResCode())
            .append("resMsg", getResMsg())
            .append("repeatCount", getRepeatCount())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
