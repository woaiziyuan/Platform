package com.platform.store.domain;

import com.platform.common.annotation.Excel;
import com.platform.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 商品主对象 store_goods
 * 
 * @author platform
 * @date 2020-03-09
 */
public class StoreGoods extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 商品id */
    private Integer goodsId;

    /** 分类id */
    @Excel(name = "分类id")
    private Long cateId;

    /** 扩展分类id */
    @Excel(name = "扩展分类id")
    private Long extendCatId;

    /** 渠道 */
    @Excel(name = "渠道")
    private Long channleId;

    /** 商品编号 */
    @Excel(name = "商品编号")
    private String goodsSn;

    /** 商品编码 */
    @Excel(name = "商品编码")
    private String goodsCode;

    /** 商品名称 */
    @Excel(name = "商品名称")
    private String goodsName;

    /** 平台商品名称 */
    @Excel(name = "平台商品名称")
    private String platformName;

    /** 品牌id */
    @Excel(name = "品牌id")
    private Integer brandId;

    /** 库存数量 */
    @Excel(name = "库存数量")
    private Integer storeCount;

    /** 市场价(零售价) */
    @Excel(name = "市场价(零售价)")
    private Double marketPrice;

    /** 本店价(供货价) */
    @Excel(name = "本店价(供货价)")
    private Double shopPrice;

    /** 商品成本价 */
    @Excel(name = "商品成本价")
    private Double costPrice;

    /** 平台结算金额 */
    @Excel(name = "平台结算金额")
    private Double platformPrice;

    /** 平台商品url */
    @Excel(name = "平台商品url")
    private String platformUrl;

    /** 用户结算金额 */
    @Excel(name = "用户结算金额")
    private Double cashAmount;

    /** 所需积分 */
    @Excel(name = "所需积分")
    private String integral;

    /** 商品关键词 */
    @Excel(name = "商品关键词")
    private String keywords;

    /** 商品简单描述 */
    @Excel(name = "商品简单描述")
    private String goodsRemark;

    /** 商品详细描述 */
    @Excel(name = "商品详细描述")
    private String goodsContent;

    /** 商品上传原始图(主图) */
    @Excel(name = "商品上传原始图(主图)")
    private String goodsLogo;

    /** 虚拟商品 */
    @Excel(name = "虚拟商品")
    private Integer virtual;

    /** 虚拟商品有效期 */
    @Excel(name = "虚拟商品有效期")
    private String virtualIndate;

    /** 虚拟商品购买上限 */
    @Excel(name = "虚拟商品购买上限")
    private Integer virtualLimit;

    /** 许过期退款 */
    @Excel(name = "许过期退款")
    private Integer virtualRefund;

    /** 上架 */
    @Excel(name = "上架")
    private Integer onSale;

    /** 包邮 */
    @Excel(name = "包邮")
    private Integer freeShipping;

    /** 商品上架时间 */
    @Excel(name = "商品上架时间")
    private Integer onTime;

    /** 商品排序 */
    @Excel(name = "商品排序")
    private Integer sort;

    /** 推荐 */
    @Excel(name = "推荐")
    private Integer recommend;

    /** 新品 */
    @Excel(name = "新品")
    private Integer newArrival;

    /** 热卖 */
    @Excel(name = "热卖")
    private Integer hot;

    /** 最后更新时间 */
    @Excel(name = "最后更新时间")
    private Integer lastUpdate;

    /** 商品所属类型id，取值表goods_type的id */
    @Excel(name = "商品所属类型id，取值表goods_type的id")
    private Integer goodsType;

    /** 商品规格类型，取值表goods_type的id */
    @Excel(name = "商品规格类型，取值表goods_type的id")
    private Integer specType;

    /** 积分兑换 */
    @Excel(name = "积分兑换")
    private Integer exchangeIntegral;

    /** 供货商ID */
    @Excel(name = "供货商ID")
    private Integer suppliersId;

    /** 商品销量 */
    @Excel(name = "商品销量")
    private Long salesSum;

    /** 属性类型 */
    @Excel(name = "属性类型")
    private Integer promType;

    /** 佣金用于分销分成 */
    @Excel(name = "佣金用于分销分成")
    private Double commission;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setGoodsId(Integer goodsId) 
    {
        this.goodsId = goodsId;
    }

    public Integer getGoodsId() 
    {
        return goodsId;
    }
    public void setCateId(Long cateId) 
    {
        this.cateId = cateId;
    }

    public Long getCateId() 
    {
        return cateId;
    }
    public void setExtendCatId(Long extendCatId) 
    {
        this.extendCatId = extendCatId;
    }

    public Long getExtendCatId() 
    {
        return extendCatId;
    }
    public void setChannleId(Long channleId) 
    {
        this.channleId = channleId;
    }

    public Long getChannleId() 
    {
        return channleId;
    }
    public void setGoodsSn(String goodsSn) 
    {
        this.goodsSn = goodsSn;
    }

    public String getGoodsSn() 
    {
        return goodsSn;
    }
    public void setGoodsCode(String goodsCode) 
    {
        this.goodsCode = goodsCode;
    }

    public String getGoodsCode() 
    {
        return goodsCode;
    }
    public void setGoodsName(String goodsName) 
    {
        this.goodsName = goodsName;
    }

    public String getGoodsName() 
    {
        return goodsName;
    }
    public void setPlatformName(String platformName) 
    {
        this.platformName = platformName;
    }

    public String getPlatformName() 
    {
        return platformName;
    }
    public void setBrandId(Integer brandId) 
    {
        this.brandId = brandId;
    }

    public Integer getBrandId() 
    {
        return brandId;
    }
    public void setStoreCount(Integer storeCount) 
    {
        this.storeCount = storeCount;
    }

    public Integer getStoreCount() 
    {
        return storeCount;
    }
    public void setMarketPrice(Double marketPrice) 
    {
        this.marketPrice = marketPrice;
    }

    public Double getMarketPrice() 
    {
        return marketPrice;
    }
    public void setShopPrice(Double shopPrice) 
    {
        this.shopPrice = shopPrice;
    }

    public Double getShopPrice() 
    {
        return shopPrice;
    }
    public void setCostPrice(Double costPrice) 
    {
        this.costPrice = costPrice;
    }

    public Double getCostPrice() 
    {
        return costPrice;
    }
    public void setPlatformPrice(Double platformPrice) 
    {
        this.platformPrice = platformPrice;
    }

    public Double getPlatformPrice() 
    {
        return platformPrice;
    }
    public void setPlatformUrl(String platformUrl) 
    {
        this.platformUrl = platformUrl;
    }

    public String getPlatformUrl() 
    {
        return platformUrl;
    }
    public void setCashAmount(Double cashAmount) 
    {
        this.cashAmount = cashAmount;
    }

    public Double getCashAmount() 
    {
        return cashAmount;
    }
    public void setIntegral(String integral) 
    {
        this.integral = integral;
    }

    public String getIntegral() 
    {
        return integral;
    }
    public void setKeywords(String keywords) 
    {
        this.keywords = keywords;
    }

    public String getKeywords() 
    {
        return keywords;
    }
    public void setGoodsRemark(String goodsRemark) 
    {
        this.goodsRemark = goodsRemark;
    }

    public String getGoodsRemark() 
    {
        return goodsRemark;
    }
    public void setGoodsContent(String goodsContent) 
    {
        this.goodsContent = goodsContent;
    }

    public String getGoodsContent() 
    {
        return goodsContent;
    }
    public void setGoodsLogo(String goodsLogo) 
    {
        this.goodsLogo = goodsLogo;
    }

    public String getGoodsLogo() 
    {
        return goodsLogo;
    }
    public void setVirtual(Integer virtual) 
    {
        this.virtual = virtual;
    }

    public Integer getVirtual() 
    {
        return virtual;
    }
    public void setVirtualIndate(String virtualIndate) 
    {
        this.virtualIndate = virtualIndate;
    }

    public String getVirtualIndate() 
    {
        return virtualIndate;
    }
    public void setVirtualLimit(Integer virtualLimit) 
    {
        this.virtualLimit = virtualLimit;
    }

    public Integer getVirtualLimit() 
    {
        return virtualLimit;
    }
    public void setVirtualRefund(Integer virtualRefund) 
    {
        this.virtualRefund = virtualRefund;
    }

    public Integer getVirtualRefund() 
    {
        return virtualRefund;
    }
    public void setOnSale(Integer onSale) 
    {
        this.onSale = onSale;
    }

    public Integer getOnSale() 
    {
        return onSale;
    }
    public void setFreeShipping(Integer freeShipping) 
    {
        this.freeShipping = freeShipping;
    }

    public Integer getFreeShipping() 
    {
        return freeShipping;
    }
    public void setOnTime(Integer onTime) 
    {
        this.onTime = onTime;
    }

    public Integer getOnTime() 
    {
        return onTime;
    }
    public void setSort(Integer sort) 
    {
        this.sort = sort;
    }

    public Integer getSort() 
    {
        return sort;
    }
    public void setRecommend(Integer recommend) 
    {
        this.recommend = recommend;
    }

    public Integer getRecommend() 
    {
        return recommend;
    }
    public void setNewArrival(Integer newArrival) 
    {
        this.newArrival = newArrival;
    }

    public Integer getNewArrival() 
    {
        return newArrival;
    }
    public void setHot(Integer hot) 
    {
        this.hot = hot;
    }

    public Integer getHot() 
    {
        return hot;
    }
    public void setLastUpdate(Integer lastUpdate) 
    {
        this.lastUpdate = lastUpdate;
    }

    public Integer getLastUpdate() 
    {
        return lastUpdate;
    }
    public void setGoodsType(Integer goodsType) 
    {
        this.goodsType = goodsType;
    }

    public Integer getGoodsType() 
    {
        return goodsType;
    }
    public void setSpecType(Integer specType) 
    {
        this.specType = specType;
    }

    public Integer getSpecType() 
    {
        return specType;
    }
    public void setExchangeIntegral(Integer exchangeIntegral) 
    {
        this.exchangeIntegral = exchangeIntegral;
    }

    public Integer getExchangeIntegral() 
    {
        return exchangeIntegral;
    }
    public void setSuppliersId(Integer suppliersId) 
    {
        this.suppliersId = suppliersId;
    }

    public Integer getSuppliersId() 
    {
        return suppliersId;
    }
    public void setSalesSum(Long salesSum) 
    {
        this.salesSum = salesSum;
    }

    public Long getSalesSum() 
    {
        return salesSum;
    }
    public void setPromType(Integer promType) 
    {
        this.promType = promType;
    }

    public Integer getPromType() 
    {
        return promType;
    }
    public void setCommission(Double commission) 
    {
        this.commission = commission;
    }

    public Double getCommission() 
    {
        return commission;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("goodsId", getGoodsId())
            .append("cateId", getCateId())
            .append("extendCatId", getExtendCatId())
            .append("channleId", getChannleId())
            .append("goodsSn", getGoodsSn())
            .append("goodsCode", getGoodsCode())
            .append("goodsName", getGoodsName())
            .append("platformName", getPlatformName())
            .append("brandId", getBrandId())
            .append("storeCount", getStoreCount())
            .append("marketPrice", getMarketPrice())
            .append("shopPrice", getShopPrice())
            .append("costPrice", getCostPrice())
            .append("platformPrice", getPlatformPrice())
            .append("platformUrl", getPlatformUrl())
            .append("cashAmount", getCashAmount())
            .append("integral", getIntegral())
            .append("keywords", getKeywords())
            .append("goodsRemark", getGoodsRemark())
            .append("goodsContent", getGoodsContent())
            .append("goodsLogo", getGoodsLogo())
            .append("virtual", getVirtual())
            .append("virtualIndate", getVirtualIndate())
            .append("virtualLimit", getVirtualLimit())
            .append("virtualRefund", getVirtualRefund())
            .append("onSale", getOnSale())
            .append("freeShipping", getFreeShipping())
            .append("onTime", getOnTime())
            .append("sort", getSort())
            .append("recommend", getRecommend())
            .append("newArrival", getNewArrival())
            .append("hot", getHot())
            .append("lastUpdate", getLastUpdate())
            .append("goodsType", getGoodsType())
            .append("specType", getSpecType())
            .append("exchangeIntegral", getExchangeIntegral())
            .append("suppliersId", getSuppliersId())
            .append("salesSum", getSalesSum())
            .append("promType", getPromType())
            .append("commission", getCommission())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .append("delFlag", getDelFlag())
            .toString();
    }
}
