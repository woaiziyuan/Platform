package com.platform.store.domain;

import com.platform.common.annotation.Excel;
import com.platform.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 手机验证码对象 store_code
 * 
 * @author platform
 * @date 2020-03-04
 */
public class StoreCode extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** null */
    private Integer codeId;

    /** 手机号 */
    @Excel(name = "手机号")
    private String phone;

    /** 验证码 */
    @Excel(name = "验证码")
    private String code;

    /** 类型 */
    @Excel(name = "类型")
    private Integer type;

    public void setCodeId(Integer codeId) 
    {
        this.codeId = codeId;
    }

    public Integer getCodeId() 
    {
        return codeId;
    }
    public void setPhone(String phone) 
    {
        this.phone = phone;
    }

    public String getPhone() 
    {
        return phone;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setType(Integer type) 
    {
        this.type = type;
    }

    public Integer getType() 
    {
        return type;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("codeId", getCodeId())
            .append("phone", getPhone())
            .append("code", getCode())
            .append("type", getType())
            .append("createTime", getCreateTime())
            .toString();
    }
}
