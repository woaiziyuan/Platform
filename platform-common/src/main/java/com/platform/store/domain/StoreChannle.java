package com.platform.store.domain;

import com.platform.common.annotation.Excel;
import com.platform.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 渠道信息对象 store_channle
 * 
 * @author platform
 * @date 2020-03-03
 */
public class StoreChannle extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 编码 */
    @Excel(name = "编码")
    private String code;

    /** 在平台方编码 */
    @Excel(name = "在平台方编码")
    private String memberCode;

    /** 渠道名称 */
    @Excel(name = "渠道名称")
    private String name;

    /** 描述 */
    @Excel(name = "描述")
    private String channleDesc;

    /** 状态 */
    @Excel(name = "状态")
    private Integer status;

    /** md5 */
    @Excel(name = "md5")
    private String md5Key;

    /** des */
    @Excel(name = "des")
    private String desKey;

    /** rsa公钥 */
    @Excel(name = "rsa公钥")
    private String pubRsaKey;

    /** rsa私钥 */
    @Excel(name = "rsa私钥")
    private String priRsaKey;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setMemberCode(String memberCode) 
    {
        this.memberCode = memberCode;
    }

    public String getMemberCode() 
    {
        return memberCode;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setChannleDesc(String channleDesc) 
    {
        this.channleDesc = channleDesc;
    }

    public String getChannleDesc() 
    {
        return channleDesc;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setMd5Key(String md5Key) 
    {
        this.md5Key = md5Key;
    }

    public String getMd5Key() 
    {
        return md5Key;
    }
    public void setDesKey(String desKey) 
    {
        this.desKey = desKey;
    }

    public String getDesKey() 
    {
        return desKey;
    }
    public void setPubRsaKey(String pubRsaKey) 
    {
        this.pubRsaKey = pubRsaKey;
    }

    public String getPubRsaKey() 
    {
        return pubRsaKey;
    }
    public void setPriRsaKey(String priRsaKey) 
    {
        this.priRsaKey = priRsaKey;
    }

    public String getPriRsaKey() 
    {
        return priRsaKey;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("code", getCode())
            .append("memberCode", getMemberCode())
            .append("name", getName())
            .append("channleDesc", getChannleDesc())
            .append("status", getStatus())
            .append("md5Key", getMd5Key())
            .append("desKey", getDesKey())
            .append("pubRsaKey", getPubRsaKey())
            .append("priRsaKey", getPriRsaKey())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
