package com.platform.store.domain;

import com.platform.common.annotation.Excel;
import com.platform.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 订单日志对象 store_order_action
 * 
 * @author platform
 * @date 2020-02-25
 */
public class StoreOrderAction extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 表id */
    private Integer actionId;

    /** 订单ID */
    @Excel(name = "订单ID")
    private Integer orderId;

    /** 操作人 0为顾客 其余为管理员 */
    @Excel(name = "操作人 0为顾客 其余为管理员")
    private Long actionUser;

    /** 订单状态(0-提交订单;1-已发货;2-发货成功;3-发货失败;4-已完成) */
    @Excel(name = "订单状态(0-提交订单;1-已发货;2-发货成功;3-发货失败;4-已完成)")
    private Integer orderStatus;

    /** 配送状态 */
    @Excel(name = "配送状态")
    private Integer shippingStatus;

    /** 支付状态 */
    @Excel(name = "支付状态")
    private Integer payStatus;

    /** 操作备注 */
    @Excel(name = "操作备注")
    private String actionNote;

    /** $column.columnComment */
    @Excel(name = "操作备注", width = 30, dateFormat = "yyyy-MM-dd")
    private Date logTime;

    /** 状态描述 */
    @Excel(name = "状态描述")
    private String statusDesc;

    public void setActionId(Integer actionId) 
    {
        this.actionId = actionId;
    }

    public Integer getActionId() 
    {
        return actionId;
    }
    public void setOrderId(Integer orderId) 
    {
        this.orderId = orderId;
    }

    public Integer getOrderId() 
    {
        return orderId;
    }
    public void setActionUser(Long actionUser) 
    {
        this.actionUser = actionUser;
    }

    public Long getActionUser() 
    {
        return actionUser;
    }
    public void setOrderStatus(Integer orderStatus) 
    {
        this.orderStatus = orderStatus;
    }

    public Integer getOrderStatus() 
    {
        return orderStatus;
    }
    public void setShippingStatus(Integer shippingStatus) 
    {
        this.shippingStatus = shippingStatus;
    }

    public Integer getShippingStatus() 
    {
        return shippingStatus;
    }
    public void setPayStatus(Integer payStatus) 
    {
        this.payStatus = payStatus;
    }

    public Integer getPayStatus() 
    {
        return payStatus;
    }
    public void setActionNote(String actionNote) 
    {
        this.actionNote = actionNote;
    }

    public String getActionNote() 
    {
        return actionNote;
    }
    public void setLogTime(Date logTime) 
    {
        this.logTime = logTime;
    }

    public Date getLogTime() 
    {
        return logTime;
    }
    public void setStatusDesc(String statusDesc) 
    {
        this.statusDesc = statusDesc;
    }

    public String getStatusDesc() 
    {
        return statusDesc;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("actionId", getActionId())
            .append("orderId", getOrderId())
            .append("actionUser", getActionUser())
            .append("orderStatus", getOrderStatus())
            .append("shippingStatus", getShippingStatus())
            .append("payStatus", getPayStatus())
            .append("actionNote", getActionNote())
            .append("logTime", getLogTime())
            .append("statusDesc", getStatusDesc())
            .toString();
    }
}
