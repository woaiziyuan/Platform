package com.platform.store.domain;

import com.platform.common.annotation.Excel;
import com.platform.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 订单商品与订单主关联对象 store_order_goods
 * 
 * @author platform
 * @date 2020-03-09
 */
public class StoreOrderGoods extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 表id自增 */
    private Integer id;

    /** 订单id */
    @Excel(name = "订单id")
    private Integer orderId;

    /** 商品id */
    @Excel(name = "商品id")
    private Integer goodsId;

    /** 商品名称 */
    @Excel(name = "商品名称")
    private String goodsName;

    /** 商品货号 */
    @Excel(name = "商品货号")
    private String goodsSn;

    /** 商品编码 */
    @Excel(name = "商品编码")
    private String goodsCode;

    /** 购买数量 */
    @Excel(name = "购买数量")
    private Integer goodsNum;

    /** 市场价 */
    @Excel(name = "市场价")
    private Double marketPrice;

    /** 本店价(供货价) */
    @Excel(name = "本店价(供货价)")
    private Double goodsPrice;

    /** 商品成本价 */
    @Excel(name = "商品成本价")
    private Double costPrice;

    /** 0 普通订单,1 限时抢购, 2 团购 , 3 促销优惠,4预售 */
    @Excel(name = "0 普通订单,1 限时抢购, 2 团购 , 3 促销优惠,4预售")
    private Integer promType;

    /** 0未发货，1已发货，2已换货，3已退货 */
    @Excel(name = "0未发货，1已发货，2已换货，3已退货")
    private Integer isSend;

    public void setId(Integer id) 
    {
        this.id = id;
    }

    public Integer getId() 
    {
        return id;
    }
    public void setOrderId(Integer orderId) 
    {
        this.orderId = orderId;
    }

    public Integer getOrderId() 
    {
        return orderId;
    }
    public void setGoodsId(Integer goodsId)
    {
        this.goodsId = goodsId;
    }

    public Integer getGoodsId()
    {
        return goodsId;
    }
    public void setGoodsName(String goodsName) 
    {
        this.goodsName = goodsName;
    }

    public String getGoodsName() 
    {
        return goodsName;
    }
    public void setGoodsSn(String goodsSn) 
    {
        this.goodsSn = goodsSn;
    }

    public String getGoodsSn() 
    {
        return goodsSn;
    }
    public void setGoodsCode(String goodsCode) 
    {
        this.goodsCode = goodsCode;
    }

    public String getGoodsCode() 
    {
        return goodsCode;
    }
    public void setGoodsNum(Integer goodsNum) 
    {
        this.goodsNum = goodsNum;
    }

    public Integer getGoodsNum() 
    {
        return goodsNum;
    }
    public void setMarketPrice(Double marketPrice) 
    {
        this.marketPrice = marketPrice;
    }

    public Double getMarketPrice() 
    {
        return marketPrice;
    }
    public void setGoodsPrice(Double goodsPrice) 
    {
        this.goodsPrice = goodsPrice;
    }

    public Double getGoodsPrice() 
    {
        return goodsPrice;
    }
    public void setCostPrice(Double costPrice) 
    {
        this.costPrice = costPrice;
    }

    public Double getCostPrice() 
    {
        return costPrice;
    }
    public void setPromType(Integer promType) 
    {
        this.promType = promType;
    }

    public Integer getPromType() 
    {
        return promType;
    }
    public void setIsSend(Integer isSend) 
    {
        this.isSend = isSend;
    }

    public Integer getIsSend() 
    {
        return isSend;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("orderId", getOrderId())
            .append("goodsId", getGoodsId())
            .append("goodsName", getGoodsName())
            .append("goodsSn", getGoodsSn())
            .append("goodsCode", getGoodsCode())
            .append("goodsNum", getGoodsNum())
            .append("marketPrice", getMarketPrice())
            .append("goodsPrice", getGoodsPrice())
            .append("costPrice", getCostPrice())
            .append("promType", getPromType())
            .append("isSend", getIsSend())
            .toString();
    }
}
