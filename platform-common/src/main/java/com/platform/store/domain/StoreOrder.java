package com.platform.store.domain;

import com.platform.common.annotation.Excel;
import com.platform.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 订单表对象 store_order
 * 
 * @author platform
 * @date 2020-03-04
 */
public class StoreOrder extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 订单id */
    private Integer orderId;

    /** 订单编号 */
    @Excel(name = "订单编号")
    private String orderSn;

    /** 订单状态 */
    @Excel(name = "订单状态")
    private Integer orderStatus;

    /** 发货状态 */
    @Excel(name = "发货状态")
    private Integer shippingStatus;

    /** 支付状态 */
    @Excel(name = "支付状态")
    private Integer payStatus;

    /** 短信发送状态 */
    @Excel(name = "短信发送状态")
    private Integer msgStatus;

    /** 收货人 */
    @Excel(name = "收货人")
    private String receiver;

    /** 国家 */
    @Excel(name = "国家")
    private Long country;

    /** 省份 */
    @Excel(name = "省份")
    private Long province;

    /** 城市 */
    @Excel(name = "城市")
    private Long city;

    /** 县区 */
    @Excel(name = "县区")
    private Long district;

    /** 乡镇 */
    @Excel(name = "乡镇")
    private Long twon;

    /** 地区地址 */
    @Excel(name = "地区地址")
    private String cityInfo;

    /** 地址详细地址 */
    @Excel(name = "地址详细地址")
    private String address;

    /** 邮政编码 */
    @Excel(name = "邮政编码")
    private String zipcode;

    /** 收货人手机号 */
    @Excel(name = "收货人手机号")
    private String receiverPhone;

    /** 联系人手机号 */
    @Excel(name = "联系人手机号")
    private String contecPhone;

    /** 邮件 */
    @Excel(name = "邮件")
    private String email;

    /** 收货方式 */
    @Excel(name = "收货方式")
    private Integer receiveType;

    /** 发货单号 */
    @Excel(name = "发货单号")
    private String shippingCode;

    /** 物流名称 */
    @Excel(name = "物流名称")
    private String shippingName;

    /** 订单渠道 */
    @Excel(name = "订单渠道")
    private Integer orderPlatform;

    /** 支付方式名称 */
    @Excel(name = "支付方式名称")
    private String payName;

    /** 商品总价 */
    @Excel(name = "商品总价")
    private Double goodsPrice;

    /** 使用积分 */
    @Excel(name = "使用积分")
    private String integral;

    /** 订单总价 */
    @Excel(name = "订单总价")
    private Double totalAmount;

    /** 下单时间 */
    @Excel(name = "下单时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date addTime;

    /** 最后新发货时间 */
    @Excel(name = "最后新发货时间")
    private Long shippingTime;

    /** 收货确认时间 */
    @Excel(name = "收货确认时间")
    private Integer confirmTime;

    /** 第三方平台交易流水号 */
    @Excel(name = "第三方平台交易流水号")
    private String transactionId;

    /** 订单类型 */
    @Excel(name = "订单类型")
    private Integer promType;

    /** 父单单号 */
    @Excel(name = "父单单号")
    private String parentSn;

    /** 失败原因 */
    @Excel(name = "失败原因")
    private String failReason;

    /** 用户假删除标识,1:删除,0未删除 */
    @Excel(name = "用户假删除标识,1:删除,0未删除")
    private Integer del;

    /** 删除|取消时间 */
    @Excel(name = "删除|取消时间")
    private Long delTime;

    public void setOrderId(Integer orderId) 
    {
        this.orderId = orderId;
    }

    public Integer getOrderId() 
    {
        return orderId;
    }
    public void setOrderSn(String orderSn) 
    {
        this.orderSn = orderSn;
    }

    public String getOrderSn() 
    {
        return orderSn;
    }
    public void setOrderStatus(Integer orderStatus) 
    {
        this.orderStatus = orderStatus;
    }

    public Integer getOrderStatus() 
    {
        return orderStatus;
    }
    public void setShippingStatus(Integer shippingStatus) 
    {
        this.shippingStatus = shippingStatus;
    }

    public Integer getShippingStatus() 
    {
        return shippingStatus;
    }
    public void setPayStatus(Integer payStatus) 
    {
        this.payStatus = payStatus;
    }

    public Integer getPayStatus() 
    {
        return payStatus;
    }
    public void setMsgStatus(Integer msgStatus) 
    {
        this.msgStatus = msgStatus;
    }

    public Integer getMsgStatus() 
    {
        return msgStatus;
    }
    public void setReceiver(String receiver) 
    {
        this.receiver = receiver;
    }

    public String getReceiver() 
    {
        return receiver;
    }
    public void setCountry(Long country) 
    {
        this.country = country;
    }

    public Long getCountry() 
    {
        return country;
    }
    public void setProvince(Long province) 
    {
        this.province = province;
    }

    public Long getProvince() 
    {
        return province;
    }
    public void setCity(Long city) 
    {
        this.city = city;
    }

    public Long getCity() 
    {
        return city;
    }
    public void setDistrict(Long district) 
    {
        this.district = district;
    }

    public Long getDistrict() 
    {
        return district;
    }
    public void setTwon(Long twon) 
    {
        this.twon = twon;
    }

    public Long getTwon() 
    {
        return twon;
    }
    public void setCityInfo(String cityInfo) 
    {
        this.cityInfo = cityInfo;
    }

    public String getCityInfo() 
    {
        return cityInfo;
    }
    public void setAddress(String address) 
    {
        this.address = address;
    }

    public String getAddress() 
    {
        return address;
    }
    public void setZipcode(String zipcode) 
    {
        this.zipcode = zipcode;
    }

    public String getZipcode() 
    {
        return zipcode;
    }
    public void setReceiverPhone(String receiverPhone) 
    {
        this.receiverPhone = receiverPhone;
    }

    public String getReceiverPhone() 
    {
        return receiverPhone;
    }
    public void setContecPhone(String contecPhone) 
    {
        this.contecPhone = contecPhone;
    }

    public String getContecPhone() 
    {
        return contecPhone;
    }
    public void setEmail(String email) 
    {
        this.email = email;
    }

    public String getEmail() 
    {
        return email;
    }
    public void setReceiveType(Integer receiveType) 
    {
        this.receiveType = receiveType;
    }

    public Integer getReceiveType() 
    {
        return receiveType;
    }
    public void setShippingCode(String shippingCode) 
    {
        this.shippingCode = shippingCode;
    }

    public String getShippingCode() 
    {
        return shippingCode;
    }
    public void setShippingName(String shippingName) 
    {
        this.shippingName = shippingName;
    }

    public String getShippingName() 
    {
        return shippingName;
    }
    public void setOrderPlatform(Integer orderPlatform) 
    {
        this.orderPlatform = orderPlatform;
    }

    public Integer getOrderPlatform() 
    {
        return orderPlatform;
    }
    public void setPayName(String payName) 
    {
        this.payName = payName;
    }

    public String getPayName() 
    {
        return payName;
    }
    public void setGoodsPrice(Double goodsPrice) 
    {
        this.goodsPrice = goodsPrice;
    }

    public Double getGoodsPrice() 
    {
        return goodsPrice;
    }
    public void setIntegral(String integral) 
    {
        this.integral = integral;
    }

    public String getIntegral() 
    {
        return integral;
    }
    public void setTotalAmount(Double totalAmount) 
    {
        this.totalAmount = totalAmount;
    }

    public Double getTotalAmount() 
    {
        return totalAmount;
    }
    public void setAddTime(Date addTime) 
    {
        this.addTime = addTime;
    }

    public Date getAddTime() 
    {
        return addTime;
    }
    public void setShippingTime(Long shippingTime) 
    {
        this.shippingTime = shippingTime;
    }

    public Long getShippingTime() 
    {
        return shippingTime;
    }
    public void setConfirmTime(Integer confirmTime) 
    {
        this.confirmTime = confirmTime;
    }

    public Integer getConfirmTime() 
    {
        return confirmTime;
    }
    public void setTransactionId(String transactionId) 
    {
        this.transactionId = transactionId;
    }

    public String getTransactionId() 
    {
        return transactionId;
    }
    public void setPromType(Integer promType) 
    {
        this.promType = promType;
    }

    public Integer getPromType() 
    {
        return promType;
    }
    public void setParentSn(String parentSn) 
    {
        this.parentSn = parentSn;
    }

    public String getParentSn() 
    {
        return parentSn;
    }
    public void setFailReason(String failReason) 
    {
        this.failReason = failReason;
    }

    public String getFailReason() 
    {
        return failReason;
    }
    public void setDel(Integer del) 
    {
        this.del = del;
    }

    public Integer getDel() 
    {
        return del;
    }
    public void setDelTime(Long delTime) 
    {
        this.delTime = delTime;
    }

    public Long getDelTime() 
    {
        return delTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("orderId", getOrderId())
            .append("orderSn", getOrderSn())
            .append("orderStatus", getOrderStatus())
            .append("shippingStatus", getShippingStatus())
            .append("payStatus", getPayStatus())
            .append("msgStatus", getMsgStatus())
            .append("receiver", getReceiver())
            .append("country", getCountry())
            .append("province", getProvince())
            .append("city", getCity())
            .append("district", getDistrict())
            .append("twon", getTwon())
            .append("cityInfo", getCityInfo())
            .append("address", getAddress())
            .append("zipcode", getZipcode())
            .append("receiverPhone", getReceiverPhone())
            .append("contecPhone", getContecPhone())
            .append("email", getEmail())
            .append("receiveType", getReceiveType())
            .append("shippingCode", getShippingCode())
            .append("shippingName", getShippingName())
            .append("orderPlatform", getOrderPlatform())
            .append("payName", getPayName())
            .append("goodsPrice", getGoodsPrice())
            .append("integral", getIntegral())
            .append("totalAmount", getTotalAmount())
            .append("addTime", getAddTime())
            .append("shippingTime", getShippingTime())
            .append("confirmTime", getConfirmTime())
            .append("transactionId", getTransactionId())
            .append("promType", getPromType())
            .append("parentSn", getParentSn())
            .append("failReason", getFailReason())
            .append("del", getDel())
            .append("delTime", getDelTime())
            .toString();
    }
}
