package com.platform.store.domain;

import com.platform.common.annotation.Excel;
import com.platform.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 券码对象 store_coupon
 * 
 * @author platform
 * @date 2020-03-04
 */
public class StoreCoupon extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** null */
    private Long id;

    /** 商品id */
    private String goodsId;

    /** 优惠券码 */
    @Excel(name = "优惠券码")
    private String coupon;

    /** 优惠券有效期 */
    @Excel(name = "优惠券有效期")
    private String period;

    /** 优惠券金额 */
    @Excel(name = "优惠券金额")
    private String amt;

    /** 所需积分 */
    @Excel(name = "所需积分")
    private String integral;

    /** 卡密码 */
    @Excel(name = "卡密码")
    private String cdkey;

    /** 使用状态 */
    @Excel(name = "使用状态")
    private Integer status;

    /** 使用时间 */
    @Excel(name = "使用时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date usedTime;

    /** 订单编号 */
    private String orderNum;

    /** 渠道 */
    @Excel(name = "渠道")
    private Long channelId;

    /** 订单表id */
    private Integer orderId;

    /** 订单编码 */
    @Excel(name = "订单编码")
    private String orderSn;

    /** 手机号 */
    @Excel(name = "手机号")
    private String phone;

    /** 兑换状态 */
    @Excel(name = "兑换状态")
    private Integer exchangeStatus;

    /** 兑换时间 */
    @Excel(name = "兑换时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date exchangeTime;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setGoodsId(String goodsId) 
    {
        this.goodsId = goodsId;
    }

    public String getGoodsId() 
    {
        return goodsId;
    }
    public void setCoupon(String coupon) 
    {
        this.coupon = coupon;
    }

    public String getCoupon() 
    {
        return coupon;
    }
    public void setPeriod(String period) 
    {
        this.period = period;
    }

    public String getPeriod() 
    {
        return period;
    }
    public void setAmt(String amt) 
    {
        this.amt = amt;
    }

    public String getAmt() 
    {
        return amt;
    }
    public void setIntegral(String integral) 
    {
        this.integral = integral;
    }

    public String getIntegral() 
    {
        return integral;
    }
    public void setCdkey(String cdkey) 
    {
        this.cdkey = cdkey;
    }

    public String getCdkey() 
    {
        return cdkey;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setUsedTime(Date usedTime) 
    {
        this.usedTime = usedTime;
    }

    public Date getUsedTime() 
    {
        return usedTime;
    }
    public void setOrderNum(String orderNum) 
    {
        this.orderNum = orderNum;
    }

    public String getOrderNum() 
    {
        return orderNum;
    }
    public void setChannelId(Long channelId) 
    {
        this.channelId = channelId;
    }

    public Long getChannelId() 
    {
        return channelId;
    }
    public void setOrderId(Integer orderId) 
    {
        this.orderId = orderId;
    }

    public Integer getOrderId() 
    {
        return orderId;
    }
    public void setOrderSn(String orderSn) 
    {
        this.orderSn = orderSn;
    }

    public String getOrderSn() 
    {
        return orderSn;
    }
    public void setPhone(String phone) 
    {
        this.phone = phone;
    }

    public String getPhone() 
    {
        return phone;
    }
    public void setExchangeStatus(Integer exchangeStatus) 
    {
        this.exchangeStatus = exchangeStatus;
    }

    public Integer getExchangeStatus() 
    {
        return exchangeStatus;
    }
    public void setExchangeTime(Date exchangeTime) 
    {
        this.exchangeTime = exchangeTime;
    }

    public Date getExchangeTime() 
    {
        return exchangeTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("goodsId", getGoodsId())
            .append("coupon", getCoupon())
            .append("period", getPeriod())
            .append("amt", getAmt())
            .append("integral", getIntegral())
            .append("cdkey", getCdkey())
            .append("status", getStatus())
            .append("usedTime", getUsedTime())
            .append("orderNum", getOrderNum())
            .append("channelId", getChannelId())
            .append("orderId", getOrderId())
            .append("orderSn", getOrderSn())
            .append("phone", getPhone())
            .append("exchangeStatus", getExchangeStatus())
            .append("exchangeTime", getExchangeTime())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
