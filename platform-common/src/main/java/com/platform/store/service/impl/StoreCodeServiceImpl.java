package com.platform.store.service.impl;

import java.util.List;
import com.platform.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.platform.store.mapper.StoreCodeMapper;
import com.platform.store.domain.StoreCode;
import com.platform.store.service.IStoreCodeService;
import com.platform.common.core.text.Convert;

/**
 * 手机验证码Service业务层处理
 * 
 * @author platform
 * @date 2020-03-04
 */
@Service
public class StoreCodeServiceImpl implements IStoreCodeService 
{
    @Autowired
    private StoreCodeMapper storeCodeMapper;

    /**
     * 查询手机验证码
     * 
     * @param codeId 手机验证码ID
     * @return 手机验证码
     */
    @Override
    public StoreCode selectStoreCodeById(Integer codeId)
    {
        return storeCodeMapper.selectStoreCodeById(codeId);
    }

    /**
     * 查询手机验证码列表
     * 
     * @param storeCode 手机验证码
     * @return 手机验证码
     */
    @Override
    public List<StoreCode> selectStoreCodeList(StoreCode storeCode)
    {
        return storeCodeMapper.selectStoreCodeList(storeCode);
    }

    /**
     * 新增手机验证码
     * 
     * @param storeCode 手机验证码
     * @return 结果
     */
    @Override
    public int insertStoreCode(StoreCode storeCode)
    {
        storeCode.setCreateTime(DateUtils.getNowDate());
        return storeCodeMapper.insertStoreCode(storeCode);
    }

    /**
     * 修改手机验证码
     * 
     * @param storeCode 手机验证码
     * @return 结果
     */
    @Override
    public int updateStoreCode(StoreCode storeCode)
    {
        return storeCodeMapper.updateStoreCode(storeCode);
    }

    /**
     * 删除手机验证码对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteStoreCodeByIds(String ids)
    {
        return storeCodeMapper.deleteStoreCodeByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除手机验证码信息
     * 
     * @param codeId 手机验证码ID
     * @return 结果
     */
    @Override
    public int deleteStoreCodeById(Integer codeId)
    {
        return storeCodeMapper.deleteStoreCodeById(codeId);
    }
}
