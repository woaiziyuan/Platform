package com.platform.store.service.impl;

import java.util.List;
import com.platform.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.platform.store.mapper.StoreCouponMapper;
import com.platform.store.domain.StoreCoupon;
import com.platform.store.service.IStoreCouponService;
import com.platform.common.core.text.Convert;

/**
 * 券码Service业务层处理
 * 
 * @author platform
 * @date 2020-03-04
 */
@Service
public class StoreCouponServiceImpl implements IStoreCouponService 
{
    @Autowired
    private StoreCouponMapper storeCouponMapper;

    /**
     * 查询券码
     * 
     * @param id 券码ID
     * @return 券码
     */
    @Override
    public StoreCoupon selectStoreCouponById(Long id)
    {
        return storeCouponMapper.selectStoreCouponById(id);
    }

    /**
     * 查询券码列表
     * 
     * @param storeCoupon 券码
     * @return 券码
     */
    @Override
    public List<StoreCoupon> selectStoreCouponList(StoreCoupon storeCoupon)
    {
        return storeCouponMapper.selectStoreCouponList(storeCoupon);
    }

    /**
     * 新增券码
     * 
     * @param storeCoupon 券码
     * @return 结果
     */
    @Override
    public int insertStoreCoupon(StoreCoupon storeCoupon)
    {
        storeCoupon.setCreateTime(DateUtils.getNowDate());
        return storeCouponMapper.insertStoreCoupon(storeCoupon);
    }

    /**
     * 修改券码
     * 
     * @param storeCoupon 券码
     * @return 结果
     */
    @Override
    public int updateStoreCoupon(StoreCoupon storeCoupon)
    {
        storeCoupon.setUpdateTime(DateUtils.getNowDate());
        return storeCouponMapper.updateStoreCoupon(storeCoupon);
    }

    /**
     * 删除券码对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteStoreCouponByIds(String ids)
    {
        return storeCouponMapper.deleteStoreCouponByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除券码信息
     * 
     * @param id 券码ID
     * @return 结果
     */
    @Override
    public int deleteStoreCouponById(Long id)
    {
        return storeCouponMapper.deleteStoreCouponById(id);
    }
}
