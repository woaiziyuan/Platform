package com.platform.store.service.impl;

import java.util.List;
import com.platform.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.platform.store.mapper.StoreBlacklistMapper;
import com.platform.store.domain.StoreBlacklist;
import com.platform.store.service.IStoreBlacklistService;
import com.platform.common.core.text.Convert;

/**
 * 黑名单Service业务层处理
 * 
 * @author platform
 * @date 2020-02-26
 */
@Service
public class StoreBlacklistServiceImpl implements IStoreBlacklistService 
{
    @Autowired
    private StoreBlacklistMapper storeBlacklistMapper;

    /**
     * 查询黑名单
     * 
     * @param id 黑名单ID
     * @return 黑名单
     */
    @Override
    public StoreBlacklist selectStoreBlacklistById(Integer id)
    {
        return storeBlacklistMapper.selectStoreBlacklistById(id);
    }

    /**
     * 查询黑名单列表
     * 
     * @param storeBlacklist 黑名单
     * @return 黑名单
     */
    @Override
    public List<StoreBlacklist> selectStoreBlacklistList(StoreBlacklist storeBlacklist)
    {
        return storeBlacklistMapper.selectStoreBlacklistList(storeBlacklist);
    }

    /**
     * 新增黑名单
     * 
     * @param storeBlacklist 黑名单
     * @return 结果
     */
    @Override
    public int insertStoreBlacklist(StoreBlacklist storeBlacklist)
    {
        storeBlacklist.setCreateTime(DateUtils.getNowDate());
        return storeBlacklistMapper.insertStoreBlacklist(storeBlacklist);
    }

    /**
     * 修改黑名单
     * 
     * @param storeBlacklist 黑名单
     * @return 结果
     */
    @Override
    public int updateStoreBlacklist(StoreBlacklist storeBlacklist)
    {
        storeBlacklist.setUpdateTime(DateUtils.getNowDate());
        return storeBlacklistMapper.updateStoreBlacklist(storeBlacklist);
    }

    /**
     * 删除黑名单对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteStoreBlacklistByIds(String ids)
    {
        return storeBlacklistMapper.deleteStoreBlacklistByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除黑名单信息
     * 
     * @param id 黑名单ID
     * @return 结果
     */
    @Override
    public int deleteStoreBlacklistById(Integer id)
    {
        return storeBlacklistMapper.deleteStoreBlacklistById(id);
    }
}
