package com.platform.store.service;

import com.platform.store.domain.StoreCode;
import java.util.List;

/**
 * 手机验证码Service接口
 * 
 * @author platform
 * @date 2020-03-04
 */
public interface IStoreCodeService 
{
    /**
     * 查询手机验证码
     * 
     * @param codeId 手机验证码ID
     * @return 手机验证码
     */
    public StoreCode selectStoreCodeById(Integer codeId);

    /**
     * 查询手机验证码列表
     * 
     * @param storeCode 手机验证码
     * @return 手机验证码集合
     */
    public List<StoreCode> selectStoreCodeList(StoreCode storeCode);

    /**
     * 新增手机验证码
     * 
     * @param storeCode 手机验证码
     * @return 结果
     */
    public int insertStoreCode(StoreCode storeCode);

    /**
     * 修改手机验证码
     * 
     * @param storeCode 手机验证码
     * @return 结果
     */
    public int updateStoreCode(StoreCode storeCode);

    /**
     * 批量删除手机验证码
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteStoreCodeByIds(String ids);

    /**
     * 删除手机验证码信息
     * 
     * @param codeId 手机验证码ID
     * @return 结果
     */
    public int deleteStoreCodeById(Integer codeId);
}
