package com.platform.store.service.impl;

import java.util.List;
import com.platform.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.platform.store.mapper.StoreGoodsMapper;
import com.platform.store.domain.StoreGoods;
import com.platform.store.service.IStoreGoodsService;
import com.platform.common.core.text.Convert;

/**
 * 商品主Service业务层处理
 * 
 * @author platform
 * @date 2020-03-09
 */
@Service
public class StoreGoodsServiceImpl implements IStoreGoodsService 
{
    @Autowired
    private StoreGoodsMapper storeGoodsMapper;

    /**
     * 查询商品主
     * 
     * @param goodsId 商品主ID
     * @return 商品主
     */
    @Override
    public StoreGoods selectStoreGoodsById(Integer goodsId)
    {
        return storeGoodsMapper.selectStoreGoodsById(goodsId);
    }

    /**
     * 查询商品主列表
     * 
     * @param storeGoods 商品主
     * @return 商品主
     */
    @Override
    public List<StoreGoods> selectStoreGoodsList(StoreGoods storeGoods)
    {
        return storeGoodsMapper.selectStoreGoodsList(storeGoods);
    }

    /**
     * 新增商品主
     * 
     * @param storeGoods 商品主
     * @return 结果
     */
    @Override
    public int insertStoreGoods(StoreGoods storeGoods)
    {
        storeGoods.setCreateTime(DateUtils.getNowDate());
        return storeGoodsMapper.insertStoreGoods(storeGoods);
    }

    /**
     * 修改商品主
     * 
     * @param storeGoods 商品主
     * @return 结果
     */
    @Override
    public int updateStoreGoods(StoreGoods storeGoods)
    {
        storeGoods.setUpdateTime(DateUtils.getNowDate());
        return storeGoodsMapper.updateStoreGoods(storeGoods);
    }

    /**
     * 删除商品主对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteStoreGoodsByIds(String ids)
    {
        return storeGoodsMapper.deleteStoreGoodsByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除商品主信息
     * 
     * @param goodsId 商品主ID
     * @return 结果
     */
    @Override
    public int deleteStoreGoodsById(Integer goodsId)
    {
        return storeGoodsMapper.deleteStoreGoodsById(goodsId);
    }

    /**
     * 功能描述:
     * 〈查询商品信息〉
     * @Param: [channleId, goodsCode]
     * @Return: com.platform.store.domain.StoreGoods
     * @Author: zy
     * @Date: 2020/3/9 16:08
     */
    @Override
    public StoreGoods selectByChannleIdAndCode(Integer channleId, String goodsCode) {
        return storeGoodsMapper.selectByChannleIdAndCode(channleId, goodsCode);
    }
}
