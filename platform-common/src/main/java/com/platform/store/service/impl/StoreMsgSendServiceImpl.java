package com.platform.store.service.impl;

import java.util.List;
import com.platform.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.platform.store.mapper.StoreMsgSendMapper;
import com.platform.store.domain.StoreMsgSend;
import com.platform.store.service.IStoreMsgSendService;
import com.platform.common.core.text.Convert;

/**
 * 信息发送Service业务层处理
 * 
 * @author platform
 * @date 2020-03-04
 */
@Service
public class StoreMsgSendServiceImpl implements IStoreMsgSendService 
{
    @Autowired
    private StoreMsgSendMapper storeMsgSendMapper;

    /**
     * 查询信息发送
     * 
     * @param msgId 信息发送ID
     * @return 信息发送
     */
    @Override
    public StoreMsgSend selectStoreMsgSendById(Long msgId)
    {
        return storeMsgSendMapper.selectStoreMsgSendById(msgId);
    }

    /**
     * 查询信息发送列表
     * 
     * @param storeMsgSend 信息发送
     * @return 信息发送
     */
    @Override
    public List<StoreMsgSend> selectStoreMsgSendList(StoreMsgSend storeMsgSend)
    {
        return storeMsgSendMapper.selectStoreMsgSendList(storeMsgSend);
    }

    /**
     * 新增信息发送
     * 
     * @param storeMsgSend 信息发送
     * @return 结果
     */
    @Override
    public int insertStoreMsgSend(StoreMsgSend storeMsgSend)
    {
        storeMsgSend.setCreateTime(DateUtils.getNowDate());
        return storeMsgSendMapper.insertStoreMsgSend(storeMsgSend);
    }

    /**
     * 修改信息发送
     * 
     * @param storeMsgSend 信息发送
     * @return 结果
     */
    @Override
    public int updateStoreMsgSend(StoreMsgSend storeMsgSend)
    {
        storeMsgSend.setUpdateTime(DateUtils.getNowDate());
        return storeMsgSendMapper.updateStoreMsgSend(storeMsgSend);
    }

    /**
     * 删除信息发送对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteStoreMsgSendByIds(String ids)
    {
        return storeMsgSendMapper.deleteStoreMsgSendByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除信息发送信息
     * 
     * @param msgId 信息发送ID
     * @return 结果
     */
    @Override
    public int deleteStoreMsgSendById(Long msgId)
    {
        return storeMsgSendMapper.deleteStoreMsgSendById(msgId);
    }
}
