package com.platform.store.service.impl;

import java.util.List;
import com.platform.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.platform.store.mapper.StoreChannleMapper;
import com.platform.store.domain.StoreChannle;
import com.platform.store.service.IStoreChannleService;
import com.platform.common.core.text.Convert;

/**
 * 渠道信息Service业务层处理
 * 
 * @author platform
 * @date 2020-03-03
 */
@Service
public class StoreChannleServiceImpl implements IStoreChannleService 
{
    @Autowired
    private StoreChannleMapper storeChannleMapper;

    /**
     * 查询渠道信息
     * 
     * @param id 渠道信息ID
     * @return 渠道信息
     */
    @Override
    public StoreChannle selectStoreChannleById(Long id)
    {
        return storeChannleMapper.selectStoreChannleById(id);
    }

    /**
     * 查询渠道信息列表
     * 
     * @param storeChannle 渠道信息
     * @return 渠道信息
     */
    @Override
    public List<StoreChannle> selectStoreChannleList(StoreChannle storeChannle)
    {
        return storeChannleMapper.selectStoreChannleList(storeChannle);
    }

    /**
     * 新增渠道信息
     * 
     * @param storeChannle 渠道信息
     * @return 结果
     */
    @Override
    public int insertStoreChannle(StoreChannle storeChannle)
    {
        storeChannle.setCreateTime(DateUtils.getNowDate());
        return storeChannleMapper.insertStoreChannle(storeChannle);
    }

    /**
     * 修改渠道信息
     * 
     * @param storeChannle 渠道信息
     * @return 结果
     */
    @Override
    public int updateStoreChannle(StoreChannle storeChannle)
    {
        storeChannle.setUpdateTime(DateUtils.getNowDate());
        return storeChannleMapper.updateStoreChannle(storeChannle);
    }

    /**
     * 删除渠道信息对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteStoreChannleByIds(String ids)
    {
        return storeChannleMapper.deleteStoreChannleByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除渠道信息信息
     * 
     * @param id 渠道信息ID
     * @return 结果
     */
    @Override
    public int deleteStoreChannleById(Long id)
    {
        return storeChannleMapper.deleteStoreChannleById(id);
    }
}
