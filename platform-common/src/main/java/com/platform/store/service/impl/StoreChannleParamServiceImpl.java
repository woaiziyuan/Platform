package com.platform.store.service.impl;

import java.util.List;
import com.platform.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.platform.store.mapper.StoreChannleParamMapper;
import com.platform.store.domain.StoreChannleParam;
import com.platform.store.service.IStoreChannleParamService;
import com.platform.common.core.text.Convert;

/**
 * 渠道网关参数Service业务层处理
 * 
 * @author platform
 * @date 2020-03-05
 */
@Service
public class StoreChannleParamServiceImpl implements IStoreChannleParamService 
{
    @Autowired
    private StoreChannleParamMapper storeChannleParamMapper;

    /**
     * 查询渠道网关参数
     * 
     * @param id 渠道网关参数ID
     * @return 渠道网关参数
     */
    @Override
    public StoreChannleParam selectStoreChannleParamById(Integer id)
    {
        return storeChannleParamMapper.selectStoreChannleParamById(id);
    }

    /**
     * 查询渠道网关参数列表
     * 
     * @param storeChannleParam 渠道网关参数
     * @return 渠道网关参数
     */
    @Override
    public List<StoreChannleParam> selectStoreChannleParamList(StoreChannleParam storeChannleParam)
    {
        return storeChannleParamMapper.selectStoreChannleParamList(storeChannleParam);
    }

    /**
     * 新增渠道网关参数
     * 
     * @param storeChannleParam 渠道网关参数
     * @return 结果
     */
    @Override
    public int insertStoreChannleParam(StoreChannleParam storeChannleParam)
    {
        storeChannleParam.setCreateTime(DateUtils.getNowDate());
        return storeChannleParamMapper.insertStoreChannleParam(storeChannleParam);
    }

    /**
     * 修改渠道网关参数
     * 
     * @param storeChannleParam 渠道网关参数
     * @return 结果
     */
    @Override
    public int updateStoreChannleParam(StoreChannleParam storeChannleParam)
    {
        storeChannleParam.setUpdateTime(DateUtils.getNowDate());
        return storeChannleParamMapper.updateStoreChannleParam(storeChannleParam);
    }

    /**
     * 删除渠道网关参数对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteStoreChannleParamByIds(String ids)
    {
        return storeChannleParamMapper.deleteStoreChannleParamByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除渠道网关参数信息
     * 
     * @param id 渠道网关参数ID
     * @return 结果
     */
    @Override
    public int deleteStoreChannleParamById(Integer id)
    {
        return storeChannleParamMapper.deleteStoreChannleParamById(id);
    }

    /**
     * 功能描述:
     * 〈根据channleId和GateId查询渠道网关参数〉
     * @Param: [channleId, gateId]
     * @Return: java.util.List<com.platform.store.domain.StoreChannleParam>
     * @Author: zy
     * @Date: 2020/3/5 15:54
     */
    @Override
    public List<StoreChannleParam> selectByChannleIdAndGateId(Integer channleId, String gateId) {
        return storeChannleParamMapper.selectByChannleIdAndGateId(channleId,gateId);
    }
}
