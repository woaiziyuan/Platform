package com.platform.store.service;

import com.platform.store.domain.StoreGoods;
import java.util.List;

/**
 * 商品主Service接口
 * 
 * @author platform
 * @date 2020-03-09
 */
public interface IStoreGoodsService 
{
    /**
     * 查询商品主
     * 
     * @param goodsId 商品主ID
     * @return 商品主
     */
    public StoreGoods selectStoreGoodsById(Integer goodsId);

    /**
     * 查询商品主列表
     * 
     * @param storeGoods 商品主
     * @return 商品主集合
     */
    public List<StoreGoods> selectStoreGoodsList(StoreGoods storeGoods);

    /**
     * 新增商品主
     * 
     * @param storeGoods 商品主
     * @return 结果
     */
    public int insertStoreGoods(StoreGoods storeGoods);

    /**
     * 修改商品主
     * 
     * @param storeGoods 商品主
     * @return 结果
     */
    public int updateStoreGoods(StoreGoods storeGoods);

    /**
     * 批量删除商品主
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteStoreGoodsByIds(String ids);

    /**
     * 删除商品主信息
     * 
     * @param goodsId 商品主ID
     * @return 结果
     */
    public int deleteStoreGoodsById(Integer goodsId);

    /**
     * 功能描述:
     * 〈根据渠道和编码查询商品信息〉
     * @Param: [channleId, goodsCode]
     * @Return: com.platform.store.domain.StoreGoods
     * @Author: zy
     * @Date: 2020/3/9 16:07
     */
    StoreGoods selectByChannleIdAndCode(Integer channleId, String goodsCode);
}
