package com.platform.store.service;

import com.platform.store.domain.StoreMsgSend;
import java.util.List;

/**
 * 信息发送Service接口
 * 
 * @author platform
 * @date 2020-03-04
 */
public interface IStoreMsgSendService 
{
    /**
     * 查询信息发送
     * 
     * @param msgId 信息发送ID
     * @return 信息发送
     */
    public StoreMsgSend selectStoreMsgSendById(Long msgId);

    /**
     * 查询信息发送列表
     * 
     * @param storeMsgSend 信息发送
     * @return 信息发送集合
     */
    public List<StoreMsgSend> selectStoreMsgSendList(StoreMsgSend storeMsgSend);

    /**
     * 新增信息发送
     * 
     * @param storeMsgSend 信息发送
     * @return 结果
     */
    public int insertStoreMsgSend(StoreMsgSend storeMsgSend);

    /**
     * 修改信息发送
     * 
     * @param storeMsgSend 信息发送
     * @return 结果
     */
    public int updateStoreMsgSend(StoreMsgSend storeMsgSend);

    /**
     * 批量删除信息发送
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteStoreMsgSendByIds(String ids);

    /**
     * 删除信息发送信息
     * 
     * @param msgId 信息发送ID
     * @return 结果
     */
    public int deleteStoreMsgSendById(Long msgId);
}
