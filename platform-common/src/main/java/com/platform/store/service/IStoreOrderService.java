package com.platform.store.service;

import com.platform.common.constant.CommonResult;
import com.platform.store.domain.StoreOrder;
import java.util.List;

/**
 * 订单表Service接口
 * 
 * @author platform
 * @date 2020-03-04
 */
public interface IStoreOrderService 
{
    /**
     * 查询订单表
     * 
     * @param orderId 订单表ID
     * @return 订单表
     */
    public StoreOrder selectStoreOrderById(Integer orderId);

    /**
     * 查询订单表列表
     * 
     * @param storeOrder 订单表
     * @return 订单表集合
     */
    public List<StoreOrder> selectStoreOrderList(StoreOrder storeOrder);

    /**
     * 新增订单表
     * 
     * @param storeOrder 订单表
     * @return 结果
     */
    public int insertStoreOrder(StoreOrder storeOrder);

    /**
     * 修改订单表
     * 
     * @param storeOrder 订单表
     * @return 结果
     */
    public int updateStoreOrder(StoreOrder storeOrder);

    /**
     * 批量删除订单表
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteStoreOrderByIds(String ids);

    /**
     * 删除订单表信息
     * 
     * @param orderId 订单表ID
     * @return 结果
     */
    public int deleteStoreOrderById(Integer orderId);

    /**
     * 功能描述:
     * 〈订单入库〉
     * @Param: [orderInfo]
     * @Return: com.platform.common.constant.CommonResult
     * @Author: zy
     * @Date: 2020/3/9 17:50
     */
    CommonResult process(String orderInfo);

    /**
     * 功能描述:
     * 〈订单发货〉
     * @Param: [orderInfo]
     * @Return: com.platform.common.constant.CommonResult
     * @Author: zy
     * @Date: 2020/3/9 17:50
     */
    CommonResult sendGoodsProcess(String orderInfo);
}
