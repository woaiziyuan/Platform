package com.platform.store.service.impl;

import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.platform.common.constant.CommonResult;
import com.platform.common.constant.ResultEnum;
import com.platform.common.utils.CacheDataUtil;
import com.platform.common.vo.GhItem;
import com.platform.common.vo.GhStoreOrderVO;
import com.platform.common.vo.StoreOrderVO;
import com.platform.store.domain.StoreGoods;
import com.platform.store.domain.StoreOrderGoods;
import com.platform.store.mapper.StoreOrderGoodsMapper;
import com.platform.store.service.IStoreGoodsService;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.platform.store.mapper.StoreOrderMapper;
import com.platform.store.domain.StoreOrder;
import com.platform.store.service.IStoreOrderService;
import com.platform.common.core.text.Convert;

/**
 * 订单表Service业务层处理
 * 
 * @author platform
 * @date 2020-03-04
 */
@Service
public class StoreOrderServiceImpl implements IStoreOrderService 
{
    @Autowired
    private StoreOrderMapper storeOrderMapper;

    @Autowired
    private StoreOrderGoodsMapper storeOrderGoodsMapper;

    @Autowired
    private CacheDataUtil cacheDataUtil;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 查询订单表
     * 
     * @param orderId 订单表ID
     * @return 订单表
     */
    @Override
    public StoreOrder selectStoreOrderById(Integer orderId)
    {
        return storeOrderMapper.selectStoreOrderById(orderId);
    }

    /**
     * 查询订单表列表
     * 
     * @param storeOrder 订单表
     * @return 订单表
     */
    @Override
    public List<StoreOrder> selectStoreOrderList(StoreOrder storeOrder)
    {
        return storeOrderMapper.selectStoreOrderList(storeOrder);
    }

    /**
     * 新增订单表
     * 
     * @param storeOrder 订单表
     * @return 结果
     */
    @Override
    public int insertStoreOrder(StoreOrder storeOrder)
    {
        return storeOrderMapper.insertStoreOrder(storeOrder);
    }

    /**
     * 修改订单表
     * 
     * @param storeOrder 订单表
     * @return 结果
     */
    @Override
    public int updateStoreOrder(StoreOrder storeOrder)
    {
        return storeOrderMapper.updateStoreOrder(storeOrder);
    }

    /**
     * 删除订单表对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteStoreOrderByIds(String ids)
    {
        return storeOrderMapper.deleteStoreOrderByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除订单表信息
     * 
     * @param orderId 订单表ID
     * @return 结果
     */
    @Override
    public int deleteStoreOrderById(Integer orderId)
    {
        return storeOrderMapper.deleteStoreOrderById(orderId);
    }

    /**
     * 功能描述:
     * 〈订单处理方法〉
     * @Param: [orderInfo]
     * @Return: CommonResult
     * @Author: zy
     * @Date: 2020/3/9 11:31
     */
    @Override
    public CommonResult process(String orderInfo) {
        CommonResult commonResult = new CommonResult();
        GhStoreOrderVO ghStoreOrderVO = (GhStoreOrderVO)JSON.parse(orderInfo);
        //如果不是待发货，则返回
        if(!"02".equals(ghStoreOrderVO.getStoreOrderStatus())){
            return commonResult;
        }
        //入库订单
        StoreOrder storeOrder = new StoreOrder();
        storeOrder.setOrderSn(ghStoreOrderVO.getStoreOrderNum());
        storeOrder.setOrderStatus(1);
        storeOrder.setShippingStatus(0);
        storeOrder.setPayStatus(1);
        storeOrder.setMsgStatus(0);
        storeOrder.setReceiver(ghStoreOrderVO.getReceiver());
        storeOrder.setAddress(ghStoreOrderVO.getReceiverAddress());
        storeOrder.setReceiverPhone(ghStoreOrderVO.getReceiverPhone());
        storeOrder.setContecPhone(ghStoreOrderVO.getContactPhone());
        storeOrderMapper.insertStoreOrder(storeOrder);

        //入库订单明细
        for (GhItem ghItem:ghStoreOrderVO.getItemList().getItem()){
            //查询商品信息
            StoreGoods storeGoods = cacheDataUtil.getStoreGoods(1,ghItem.getProductCode());
            if(storeGoods==null){
                commonResult.setCodeMsg(ResultEnum.FAIL,"没找到商品信息");
                return commonResult;
            }
            StoreOrderGoods storeOrderGoods = new StoreOrderGoods();
            storeOrderGoods.setOrderId(storeOrder.getOrderId());
            storeOrderGoods.setGoodsId(storeGoods.getGoodsId());
            storeOrderGoods.setGoodsName(ghItem.getItemName());
            storeOrderGoods.setGoodsSn(ghItem.getProductCode());
            storeOrderGoods.setGoodsCode(ghItem.getCommodityCode());
            storeOrderGoods.setGoodsNum(ghItem.getItemNum());
            storeOrderGoods.setIsSend(0);
            storeOrderGoodsMapper.insertStoreOrderGoods(storeOrderGoods);
        }

        //放入mq中准备发货
        rabbitTemplate.convertAndSend("ghSendGoodsQueue",JSONObject.toJSONString(storeOrder));

        return commonResult;
    }

    /**
     * 功能描述:
     * 〈订单发货〉
     * @Param: [orderInfo]
     * @Return: com.platform.common.constant.CommonResult
     * @Author: zy
     * @Date: 2020/3/9 17:51
     */
    @Override
    public CommonResult sendGoodsProcess(String orderInfo) {
        CommonResult commonResult = new CommonResult();
        StoreOrder storeOrder = (StoreOrder)JSON.parse(orderInfo);
        StoreOrderGoods storeOrderGoodsVo = new StoreOrderGoods();
        storeOrderGoodsVo.setOrderId(storeOrder.getOrderId());
        List<StoreOrderGoods> storeOrderGoodsList = storeOrderGoodsMapper.selectStoreOrderGoodsList(storeOrderGoodsVo);
        for (StoreOrderGoods storeOrderGoods:storeOrderGoodsList){
            //TODO 查询商品表发货、减库存

            //TODO 发短信

            //TODO 更改订单发货状态
        }
        return null;
    }
}
