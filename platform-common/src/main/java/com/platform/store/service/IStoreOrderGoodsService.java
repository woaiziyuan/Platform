package com.platform.store.service;

import com.platform.store.domain.StoreOrderGoods;
import java.util.List;

/**
 * 订单商品与订单主关联Service接口
 * 
 * @author platform
 * @date 2020-03-09
 */
public interface IStoreOrderGoodsService 
{
    /**
     * 查询订单商品与订单主关联
     * 
     * @param id 订单商品与订单主关联ID
     * @return 订单商品与订单主关联
     */
    public StoreOrderGoods selectStoreOrderGoodsById(Integer id);

    /**
     * 查询订单商品与订单主关联列表
     * 
     * @param storeOrderGoods 订单商品与订单主关联
     * @return 订单商品与订单主关联集合
     */
    public List<StoreOrderGoods> selectStoreOrderGoodsList(StoreOrderGoods storeOrderGoods);

    /**
     * 新增订单商品与订单主关联
     * 
     * @param storeOrderGoods 订单商品与订单主关联
     * @return 结果
     */
    public int insertStoreOrderGoods(StoreOrderGoods storeOrderGoods);

    /**
     * 修改订单商品与订单主关联
     * 
     * @param storeOrderGoods 订单商品与订单主关联
     * @return 结果
     */
    public int updateStoreOrderGoods(StoreOrderGoods storeOrderGoods);

    /**
     * 批量删除订单商品与订单主关联
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteStoreOrderGoodsByIds(String ids);

    /**
     * 删除订单商品与订单主关联信息
     * 
     * @param id 订单商品与订单主关联ID
     * @return 结果
     */
    public int deleteStoreOrderGoodsById(Integer id);
}
