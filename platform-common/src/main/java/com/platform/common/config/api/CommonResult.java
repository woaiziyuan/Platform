package com.platform.common.config.api;

import lombok.Data;

@Data
public class CommonResult<T> {
    private String code;
    private String msg;
    private T data;

    public CommonResult() {
        super();
    }

    public CommonResult(String code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public CommonResult(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public void setCodeMsg(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public void setCodeMsg(ResultEnum resultEnum) {
        this.code = resultEnum.getCode();
        this.msg = resultEnum.getMsg();
    }

    public void setCodeMsg(ResultEnum resultEnum, String msg) {
        this.code = resultEnum.getCode();
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "Result{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }
}


