package com.platform.common.config.api;

public enum ResultEnum {
    //这里是可以自己定义的，方便与前端交互即可
    UNKNOWN_ERROR("999999","未知错误"),
    SUCCESS("000000","成功"),
    FAIL("999998","失败"),
    USER_NOT_EXIST("100001","用户不存在"),
    USER_IS_EXISTS("100002","用户已存在"),
    DATA_IS_NULL("100003","数据为空"),
    PARAM_IS_NULL("100004","渠道参数为空"),
    ;
    private String code;
    private String msg;
 
    ResultEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }
 
    public String getCode() {
        return code;
    }
 
    public String getMsg() {
        return msg;
    }
}
