package com.platform.common.utils;

import com.platform.common.config.api.CommonResult;
import com.platform.common.config.api.ResultEnum;

public class CommonResultUtil {
 
    /**成功且带数据**/
    public static CommonResult success(Object object){
        CommonResult CommonResult = new CommonResult();
        CommonResult.setCode(ResultEnum.SUCCESS.getCode());
        CommonResult.setMsg(ResultEnum.SUCCESS.getMsg());
        CommonResult.setData(object);
        return CommonResult;
    }

    /**成功但不带数据**/
    public static CommonResult success(){
        return success(null);
    }

    /**失败且带数据**/
    public static CommonResult fail(Object object){
        CommonResult CommonResult = new CommonResult();
        CommonResult.setCode(ResultEnum.FAIL.getCode());
        CommonResult.setMsg(ResultEnum.FAIL.getMsg());
        CommonResult.setData(object);
        return CommonResult;
    }

    /**失败**/
    public static CommonResult error(String code,String msg){
        CommonResult CommonResult = new CommonResult();
        CommonResult.setCode(code);
        CommonResult.setMsg(msg);
        return CommonResult;
    }
}
