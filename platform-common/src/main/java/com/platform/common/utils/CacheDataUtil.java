package com.platform.common.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.platform.store.domain.StoreChannleParam;
import com.platform.store.domain.StoreGoods;
import com.platform.store.service.IStoreChannleParamService;
import com.platform.store.service.IStoreGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Description 缓存获取工具类
 * @Author zy
 * @Date 2020/3/5 15:35
 */
@Component
public class CacheDataUtil {

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private IStoreChannleParamService iStoreChannleParamService;

    @Autowired
    private IStoreGoodsService iStoreGoodsService;

    private static String channleParamKey = "StoreChannleParam:StoreChannleParam";
    private static String StoreGoodsKey = "StoreGoods:StoreGoods";


    //获取渠道参数
    public Map<String, Object> getChannleParam(Integer channleId, String gateId) {
        String key = channleParamKey + channleId + gateId;
        Map<String, Object> map = new ConcurrentHashMap<>();
        Object obj = redisUtil.get(key);
        if (obj == null) {
            synchronized (this) {
                if (redisUtil.hasKey(key)) {
                    obj = redisUtil.get(key);
                    map = JSON.parseObject((String) obj, new TypeReference<Map<String, Object>>() {});
                    return map;
                }
                //如果redis里不存在 直接从数据库获取
                List<StoreChannleParam> storeChannleParamList = iStoreChannleParamService.selectByChannleIdAndGateId(channleId,gateId);
                //TODO 这里需要处理一下，如果没查到怎么办
//                if (storeChannleParamList == null || storeChannleParamList.size() == 0) {
//
//                }
                if (storeChannleParamList != null && storeChannleParamList.size() > 0) {
                    for (StoreChannleParam storeChannleParam : storeChannleParamList) {
                        map.put(storeChannleParam.getParaKey(), storeChannleParam.getParaValue());
                    }
                }
                if (map != null && map.size() > 0) {
                    redisUtil.set(key, JSON.toJSONString(map), 86400L);
                }
            }
        } else {
            map = JSON.parseObject((String) obj, new TypeReference<Map<String, Object>>() {
            });
            return map;
        }
        return map;
    }

    //获取商品信息
    public StoreGoods getStoreGoods(Integer channleId, String goodsCode) {
        String key = StoreGoodsKey + channleId + goodsCode;
        Map<String, Object> map = new ConcurrentHashMap<>();
        StoreGoods storeGoods = null;
        Object obj = redisUtil.get(key);
        if (obj == null) {
            synchronized (this) {
                if (redisUtil.hasKey(key)) {
                    obj = redisUtil.get(key);
                    storeGoods = JSON.parseObject((String)obj, new TypeReference<StoreGoods>() {});
                    return storeGoods;
                }
                //如果redis里不存在 直接从数据库获取
                storeGoods = iStoreGoodsService.selectByChannleIdAndCode(channleId,goodsCode);
                //TODO 这里需要处理一下，如果没查到怎么办
//                if (storeChannleParamList == null || storeChannleParamList.size() == 0) {
//
//                }
                if (storeGoods != null) {
                    redisUtil.set(key,JSON.toJSONString(storeGoods),86400L);
                }
            }
        } else {
            storeGoods = JSON.parseObject((String)obj, new TypeReference<StoreGoods>() {});
        }
        return storeGoods;
    }
}
