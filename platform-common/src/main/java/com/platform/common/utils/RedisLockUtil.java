package com.platform.common.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 分布式锁工具<br/>
 * 说明:基于redis，使用setNx命令。使用value为时间戳的形式，保证不会一直持有锁
 * <p>
 * Created by xxon 2018/2/7.
 */
@Component
public class RedisLockUtil {

    @SuppressWarnings("rawtypes")
    @Resource(name = "redisTemplate")
    private RedisTemplate<Object, Object> redisTemplate;

    /**
     * 加锁默认超时时间
     */
    private final long DEFAULT_TIMEOUT_SECOND = 5;

    /**
     * 加锁循环等待时间
     */
    private final long LOOP_WAIT_TIME_MILLISECOND = 30;

    private Logger logger = LoggerFactory.getLogger(RedisLockUtil.class);

    /**
     * 加锁
     *
     * @param key
     * @param timeoutSecond 如果为null,使用默认超时时间
     * @return 加锁的值（超时时间）
     */
    public long lock(String key, Long timeoutSecond) {
        logger.info("Thread：" + Thread.currentThread().getName() + " start lock");

        if (key == null) {
            logger.info("Thread：" + Thread.currentThread().getName() + " 无法给空进行加锁！");
            return 0L;
        }
        //如果参数错误
        if (timeoutSecond != null && timeoutSecond <= 0) {
            timeoutSecond = DEFAULT_TIMEOUT_SECOND;
        }
        timeoutSecond = timeoutSecond == null ? DEFAULT_TIMEOUT_SECOND : timeoutSecond;

        if (timeoutSecond > 50)
            timeoutSecond = 50L;
        while (true) {
            //超时时间点
            long timeoutTimeMilli = currentTimeMilliForRedis() + timeoutSecond * 1000;
            //如果设置成功
            if (redisTemplate.opsForValue().setIfAbsent(key, String.valueOf(timeoutTimeMilli))) {//放入的设置为字符串
                logger.info("Thread：" + Thread.currentThread().getName() + " lock success");
                return timeoutTimeMilli;
            }
            //如果已经超时
            Long value = 0L;
            try {
                value = Long.parseLong((String) redisTemplate.opsForValue().get(key));//获取出字符串转换为long
            } catch (Exception ex) {
                continue;
            }
            if (value != null && value.longValue() < currentTimeMilliForRedis()) {
                //设置新的超时时间
                Long oldValue = Long.parseLong((String) redisTemplate.opsForValue().getAndSet(key, String.valueOf(timeoutTimeMilli)));//旧的值

                //多个线程同时getset，只有第一个才可以获取到锁
                if (value.equals(oldValue)) {
                    logger.info("Thread：" + Thread.currentThread().getName() + " lock success");
                    return timeoutTimeMilli;
                }
            }
            //延迟一定毫秒，防止请求太频繁
            try {
                Thread.sleep(LOOP_WAIT_TIME_MILLISECOND);
            } catch (InterruptedException e) {
                logger.error("DistributedLockUtil lock sleep error", e);
            }
        }
    }

    /**
     * 释放锁
     *
     * @param key
     * @param lockValue
     */
    public void unLock(String key, long lockValue) {

        logger.info("Thread：" + Thread.currentThread().getName() + " start unlock");
        Object obj = redisTemplate.opsForValue().get(key);
        if (obj == null) {
            logger.info("Thread：" + Thread.currentThread().getName() + " unlock not find :" + key);
            return;
        }
        Long value = Long.parseLong((String) obj);
        if (value != null && value.equals(lockValue)) {//如果是本线程加锁
            redisTemplate.delete(key);
            logger.info("Thread：" + Thread.currentThread().getName() + " unlock success");
        }
    }

    /**
     * redis服务器时间
     *
     * @return
     */
    private long currentTimeMilliForRedis() {

        return redisTemplate.execute(new RedisCallback<Long>() {
            @Override
            public Long doInRedis(RedisConnection connection) throws DataAccessException {
                return connection.time();
            }
        });
    }
} 