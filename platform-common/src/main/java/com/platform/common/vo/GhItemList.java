/**
 * Copyright 2020 bejson.com
 */
package com.platform.common.vo;

import lombok.Data;

import java.util.List;

/**
 * Auto-generated: 2020-03-06 15:25:49
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
@Data
public class GhItemList {
    private List<GhItem> item;
}