/**
  * Copyright 2020 bejson.com 
  */
package com.platform.common.vo;
import lombok.Data;

import java.util.Date;

/**
 * Auto-generated: 2020-03-06 15:25:49
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
@Data
public class GhStoreOrderVO {
    private String storeOrderNum;
    private String storeOrderStatus;
    private String receiver;
    private String receiverAddress;
    private String receiverPhone;
    private String contactPhone;
    private GhItemList itemList;
    private Date createTime;
    private GhVerifiedList verifiedList;
}