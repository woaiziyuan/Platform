package com.platform.common.vo;

import lombok.Data;

/**
 * @Description 订单VO类
 * @Author zy
 * @Date 2020/3/5 14:17
 */
@Data
public class StoreOrderVO {
    //渠道ID
    public Integer channleId;
    //网关ID
    public String gateId;
    //订单状态
    public String orderStatus;
    //查询区间开始
    public String startDate;
    //查询区间结束
    public String endDate;
    //记录开始编号
    public String startRowNum;
    //记录结束编号
    public String endRowNum;
}
