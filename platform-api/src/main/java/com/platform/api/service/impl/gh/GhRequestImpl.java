package com.platform.api.service.impl.gh;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.platform.api.service.IOrderService;
import com.platform.api.service.ISendMessageService;
import com.platform.api.util.*;
import com.platform.common.constant.CommonResult;
import com.platform.common.constant.ResultEnum;
import com.platform.common.utils.CacheDataUtil;
import com.platform.common.utils.MD5Util;
import com.platform.common.vo.GhStoreOrderVO;
import com.platform.common.vo.StoreOrderVO;
import com.platform.store.domain.StoreOrder;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Description 国航订单处理类
 * @Author zy
 * @Date 2020/3/5 14:22
 */
@Component
public class GhRequestImpl implements IOrderService, ISendMessageService {

    private static final Logger log = LoggerFactory.getLogger(GhRequestImpl.class);

    @Autowired
    private CacheDataUtil cacheDataUtil;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Override
    public CommonResult queryOrder(StoreOrderVO orderInfoVO) {
        CommonResult commonResult = new CommonResult();
        //获取缓存中的网关信息
        Map<String, Object> paramMap = cacheDataUtil.getChannleParam(orderInfoVO.getChannleId(), orderInfoVO.getGateId());
        if (paramMap == null) {
            commonResult.setCodeMsg(ResultEnum.PARAM_IS_NULL);
            return commonResult;
        }
        Map<String, Object> map = new ConcurrentHashMap<>(6);
        map.put("memberName", paramMap.get("memberName"));
        map.put("orderStatus", orderInfoVO.getOrderStatus());
        map.put("startDate", orderInfoVO.getStartDate());
        map.put("endDate", orderInfoVO.getEndDate());
        map.put("startRowNum", orderInfoVO.getStartRowNum());
        map.put("endRowNum", orderInfoVO.getEndRowNum());

        String bipCode = String.valueOf(paramMap.get("bipCode"));
        String businessMsg = JSON.toJSONString(map);
        String key = String.valueOf(paramMap.get("key"));
        String reqUrl = String.valueOf(paramMap.get("reqUrl"));
        String timestamp = DateUtils.getSystemTime();
        String chkValue = "timestamp=" + timestamp
                + "&bipCode=" + bipCode
                + "&businessMsg=" + businessMsg
                + key;
        StringBuffer buf = new StringBuffer(chkValue);
        try {
            String sign = MD5Util.getMD5(buf.toString().getBytes("UTF-8"));
            log.info("请求国航获取订单信息参数:" + businessMsg);
//            String reponse = HttpUtils.sendPost(reqUrl + "queryOrder?sign=" + sign + "&timestamp=" + timestamp, businessMsg);
            String reponse = "{\"tradeSum\":3,\"storeOrderList\":[{\"storeOrderNum\":\"1187539616276709376\",\"storeOrderStatus\":\"02\",\"receiver\":null,\"receiverAddress\":null,\"receiverPhone\":null,\"contactPhone\":\"15275129311\",\"itemList\":{\"item\":[{\"productCode\":\"20191017002\",\"commodityCode\":\"20191017002\",\"itemName\":\"COSTA北方区至尊型甜品代金券|/+/|单张\",\"itemNum\":1}]},\"createTime\":\"2019-10-25 09:21:22\",\"verifiedList\":null},{\"storeOrderNum\":\"1187539722258391041\",\"storeOrderStatus\":\"02\",\"receiver\":null,\"receiverAddress\":null,\"receiverPhone\":null,\"contactPhone\":\"15275129311\",\"itemList\":{\"item\":[{\"productCode\":\"20191017001\",\"commodityCode\":\"20191017001\",\"itemName\":\"COSTA北方区尊享型甜品代金券|/+/|单张\",\"itemNum\":1}]},\"createTime\":\"2019-10-25 09:21:47\",\"verifiedList\":null},{\"storeOrderNum\":\"1187539856174129152\",\"storeOrderStatus\":\"02\",\"receiver\":null,\"receiverAddress\":null,\"receiverPhone\":null,\"contactPhone\":\"15275129311\",\"itemList\":{\"item\":[{\"productCode\":\"2019082601\",\"commodityCode\":\"2019082601\",\"itemName\":\"COSTA咖啡西藏/宁夏区域专用单杯代金券|/+/|单张\",\"itemNum\":21}]},\"createTime\":\"2019-10-25 09:22:19\",\"verifiedList\":null}],\"resDesc\":\"接口调用成功\",\"resCode\":\"0000\"}";
            JSONObject jsonObjectResult = JSON.parseObject(reponse);
            if("0000".equals(jsonObjectResult.getString("resCode"))){
                if(StringUtils.isNotBlank(jsonObjectResult.getString("tradeSum"))){
                    List<GhStoreOrderVO> storeOrderList = JSONObject.parseArray(jsonObjectResult.getString("storeOrderList"), GhStoreOrderVO.class);
                    for (GhStoreOrderVO ghStoreOrderVO:storeOrderList) {
                        rabbitTemplate.convertAndSend("ghQueue",JSONObject.toJSONString(ghStoreOrderVO));
                    }
                }
                commonResult.setCodeMsg(ResultEnum.SUCCESS);
            }else{
                commonResult.setCodeMsg(ResultEnum.FAIL,"["+jsonObjectResult.getString("resCode")+"]"+jsonObjectResult.getString("resDesc"));
            }
        } catch (UnsupportedEncodingException e) {
            log.info("请求国航获取订单发生异常:" + e.getMessage());
            commonResult.setCodeMsg(ResultEnum.UNKNOWN_ERROR);
            e.printStackTrace();
        }
        return commonResult;
    }

    @Override
    public CommonResult sendGoodsNotice(StoreOrder storeOrder) {
        return null;
    }

    @Override
    public CommonResult sendMessage(StoreOrder storeOrder) {
        return null;
    }
}
