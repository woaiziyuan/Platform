package com.platform.api.service;

import com.platform.common.constant.CommonResult;
import com.platform.store.domain.StoreOrder;

public interface ISendMessageService {
    /**
     * 功能描述:
     * 〈短信发送〉
     * @Param: [storeOrder]
     * @Return: com.platform.common.config.api.CommonResult
     * @Author: zy
     * @Date: 2020/3/5 14:29
     */
    CommonResult sendMessage(StoreOrder storeOrder);
}
