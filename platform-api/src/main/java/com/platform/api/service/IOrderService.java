package com.platform.api.service;

import com.platform.common.constant.CommonResult;
import com.platform.common.vo.StoreOrderVO;
import com.platform.store.domain.StoreOrder;

/**
 * 功能描述:订单处理Service
 * 〈〉
 * @Param:
 * @Return:
 * @Author: zy
 * @Date: 2020/3/5 14:11
 */
public interface IOrderService {
    /**
     * 功能描述:
     * 〈订单查询〉
     * @Param: [orderInfoVO]
     * @Return: com.platform.common.config.api.CommonResult
     * @Author: zy
     * @Date: 2020/3/5 14:23
     */
    CommonResult queryOrder(StoreOrderVO storeOrderVO);
    /**
     * 功能描述:
     * 〈发货通知〉
     * @Param: [storeOrder]
     * @Return: com.platform.common.config.api.CommonResult
     * @Author: zy
     * @Date: 2020/3/5 14:28
     */
    CommonResult sendGoodsNotice(StoreOrder storeOrder);
}
