package com.platform.api.job;

import com.platform.api.service.IOrderService;
import com.platform.common.constant.CommonResult;
import com.platform.common.constant.ResultEnum;
import com.platform.common.utils.RedisUtil;
import com.platform.common.vo.StoreOrderVO;
import com.platform.common.utils.spring.SpringUtils;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.xxl.job.core.log.XxlJobLogger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Description 查询国航订单
 * @Author zy
 * @Date 2020/2/20 15:14
 */
@Component
public class GHQueryOrderJob {
    private static Logger logger = LoggerFactory.getLogger(GHQueryOrderJob.class);

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private RabbitTemplate rabbitTemplate;
    /**
     * 1、简单任务示例（Bean模式）
     */
    @XxlJob("ghQueryOrderJobHandler")
    public ReturnT<String> ghQueryOrderJobHandler(String param) throws Exception {
        XxlJobLogger.log("XXL-JOB, Hello World.");
        IOrderService orderService = SpringUtils.getBean("gh"+"RequestImpl");
        if(orderService==null){
            return ReturnT.FAIL;
        }
        StoreOrderVO storeOrderVO = new StoreOrderVO();
        storeOrderVO.setChannleId(1);
        storeOrderVO.setOrderStatus("02");
        storeOrderVO.setStartDate("20200201");
        storeOrderVO.setEndDate("20200202");
        storeOrderVO.setGateId("gh");
        storeOrderVO.setStartRowNum("1");
        storeOrderVO.setEndRowNum("2");
        CommonResult commonResult = orderService.queryOrder(storeOrderVO);
        if(ResultEnum.SUCCESS.getCode().equals(commonResult.getCode())){
            return ReturnT.SUCCESS;
        }else {
            return ReturnT.FAIL;
        }
    }
}
