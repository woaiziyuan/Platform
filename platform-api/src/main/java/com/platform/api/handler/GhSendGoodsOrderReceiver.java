package com.platform.api.handler;

import com.platform.common.constant.CommonResult;
import com.platform.common.constant.ResultEnum;
import com.platform.store.service.IStoreOrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = "ghSendGoodsQueue")
public class GhSendGoodsOrderReceiver {

    private static final Logger log = LoggerFactory.getLogger(GhSendGoodsOrderReceiver.class);

    @Autowired
    private IStoreOrderService iStoreOrderService;

    @RabbitHandler
    public void process(String orderInfo) {
        log.info("国航接收到待发货订单信息:"+orderInfo);
        CommonResult commonResult = iStoreOrderService.sendGoodsProcess(orderInfo);
        if(ResultEnum.SUCCESS.getCode().equals(commonResult.getCode())){

        }
    }
}