package com.platform.web.controller.store;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.platform.common.annotation.Log;
import com.platform.common.enums.BusinessType;
import com.platform.store.domain.StoreOrder;
import com.platform.store.service.IStoreOrderService;
import com.platform.common.core.controller.BaseController;
import com.platform.common.core.domain.AjaxResult;
import com.platform.common.utils.poi.ExcelUtil;
import com.platform.common.core.page.TableDataInfo;

/**
 * 订单表Controller
 * 
 * @author platform
 * @date 2020-03-04
 */
@Controller
@RequestMapping("/store/order")
public class StoreOrderController extends BaseController
{
    private String prefix = "store/order";

    @Autowired
    private IStoreOrderService storeOrderService;

    @RequiresPermissions("store:order:view")
    @GetMapping()
    public String order()
    {
        return prefix + "/order";
    }

    /**
     * 查询订单表列表
     */
    @RequiresPermissions("store:order:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(StoreOrder storeOrder)
    {
        startPage();
        List<StoreOrder> list = storeOrderService.selectStoreOrderList(storeOrder);
        return getDataTable(list);
    }

    /**
     * 导出订单表列表
     */
    @RequiresPermissions("store:order:export")
    @Log(title = "订单表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(StoreOrder storeOrder)
    {
        List<StoreOrder> list = storeOrderService.selectStoreOrderList(storeOrder);
        ExcelUtil<StoreOrder> util = new ExcelUtil<StoreOrder>(StoreOrder.class);
        return util.exportExcel(list, "order");
    }

    /**
     * 新增订单表
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存订单表
     */
    @RequiresPermissions("store:order:add")
    @Log(title = "订单表", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(StoreOrder storeOrder)
    {
        return toAjax(storeOrderService.insertStoreOrder(storeOrder));
    }

    /**
     * 修改订单表
     */
    @GetMapping("/edit/{orderId}")
    public String edit(@PathVariable("orderId") Integer orderId, ModelMap mmap)
    {
        StoreOrder storeOrder = storeOrderService.selectStoreOrderById(orderId);
        mmap.put("storeOrder", storeOrder);
        return prefix + "/edit";
    }

    /**
     * 修改保存订单表
     */
    @RequiresPermissions("store:order:edit")
    @Log(title = "订单表", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(StoreOrder storeOrder)
    {
        return toAjax(storeOrderService.updateStoreOrder(storeOrder));
    }

    /**
     * 删除订单表
     */
    @RequiresPermissions("store:order:remove")
    @Log(title = "订单表", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(storeOrderService.deleteStoreOrderByIds(ids));
    }
}
