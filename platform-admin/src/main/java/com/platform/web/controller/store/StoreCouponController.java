package com.platform.web.controller.store;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.platform.common.annotation.Log;
import com.platform.common.enums.BusinessType;
import com.platform.store.domain.StoreCoupon;
import com.platform.store.service.IStoreCouponService;
import com.platform.common.core.controller.BaseController;
import com.platform.common.core.domain.AjaxResult;
import com.platform.common.utils.poi.ExcelUtil;
import com.platform.common.core.page.TableDataInfo;

/**
 * 券码Controller
 * 
 * @author platform
 * @date 2020-03-04
 */
@Controller
@RequestMapping("/store/coupon")
public class StoreCouponController extends BaseController
{
    private String prefix = "store/coupon";

    @Autowired
    private IStoreCouponService storeCouponService;

    @RequiresPermissions("store:coupon:view")
    @GetMapping()
    public String coupon()
    {
        return prefix + "/coupon";
    }

    /**
     * 查询券码列表
     */
    @RequiresPermissions("store:coupon:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(StoreCoupon storeCoupon)
    {
        startPage();
        List<StoreCoupon> list = storeCouponService.selectStoreCouponList(storeCoupon);
        return getDataTable(list);
    }

    /**
     * 导出券码列表
     */
    @RequiresPermissions("store:coupon:export")
    @Log(title = "券码", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(StoreCoupon storeCoupon)
    {
        List<StoreCoupon> list = storeCouponService.selectStoreCouponList(storeCoupon);
        ExcelUtil<StoreCoupon> util = new ExcelUtil<StoreCoupon>(StoreCoupon.class);
        return util.exportExcel(list, "coupon");
    }

    /**
     * 新增券码
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存券码
     */
    @RequiresPermissions("store:coupon:add")
    @Log(title = "券码", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(StoreCoupon storeCoupon)
    {
        return toAjax(storeCouponService.insertStoreCoupon(storeCoupon));
    }

    /**
     * 修改券码
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        StoreCoupon storeCoupon = storeCouponService.selectStoreCouponById(id);
        mmap.put("storeCoupon", storeCoupon);
        return prefix + "/edit";
    }

    /**
     * 修改保存券码
     */
    @RequiresPermissions("store:coupon:edit")
    @Log(title = "券码", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(StoreCoupon storeCoupon)
    {
        return toAjax(storeCouponService.updateStoreCoupon(storeCoupon));
    }

    /**
     * 删除券码
     */
    @RequiresPermissions("store:coupon:remove")
    @Log(title = "券码", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(storeCouponService.deleteStoreCouponByIds(ids));
    }
}
