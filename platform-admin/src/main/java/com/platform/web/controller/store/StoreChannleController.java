package com.platform.web.controller.store;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.platform.common.annotation.Log;
import com.platform.common.enums.BusinessType;
import com.platform.store.domain.StoreChannle;
import com.platform.store.service.IStoreChannleService;
import com.platform.common.core.controller.BaseController;
import com.platform.common.core.domain.AjaxResult;
import com.platform.common.utils.poi.ExcelUtil;
import com.platform.common.core.page.TableDataInfo;

/**
 * 渠道信息Controller
 * 
 * @author platform
 * @date 2020-03-03
 */
@Controller
@RequestMapping("/store/channle")
public class StoreChannleController extends BaseController
{
    private String prefix = "store/channle";

    @Autowired
    private IStoreChannleService storeChannleService;

    @RequiresPermissions("store:channle:view")
    @GetMapping()
    public String channle()
    {
        return prefix + "/channle";
    }

    /**
     * 查询渠道信息列表
     */
    @RequiresPermissions("store:channle:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(StoreChannle storeChannle)
    {
        startPage();
        List<StoreChannle> list = storeChannleService.selectStoreChannleList(storeChannle);
        return getDataTable(list);
    }

    /**
     * 导出渠道信息列表
     */
    @RequiresPermissions("store:channle:export")
    @Log(title = "渠道信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(StoreChannle storeChannle)
    {
        List<StoreChannle> list = storeChannleService.selectStoreChannleList(storeChannle);
        ExcelUtil<StoreChannle> util = new ExcelUtil<StoreChannle>(StoreChannle.class);
        return util.exportExcel(list, "channle");
    }

    /**
     * 新增渠道信息
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存渠道信息
     */
    @RequiresPermissions("store:channle:add")
    @Log(title = "渠道信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(StoreChannle storeChannle)
    {
        return toAjax(storeChannleService.insertStoreChannle(storeChannle));
    }

    /**
     * 修改渠道信息
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        StoreChannle storeChannle = storeChannleService.selectStoreChannleById(id);
        mmap.put("storeChannle", storeChannle);
        return prefix + "/edit";
    }

    /**
     * 修改保存渠道信息
     */
    @RequiresPermissions("store:channle:edit")
    @Log(title = "渠道信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(StoreChannle storeChannle)
    {
        return toAjax(storeChannleService.updateStoreChannle(storeChannle));
    }

    /**
     * 删除渠道信息
     */
    @RequiresPermissions("store:channle:remove")
    @Log(title = "渠道信息", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(storeChannleService.deleteStoreChannleByIds(ids));
    }
}
