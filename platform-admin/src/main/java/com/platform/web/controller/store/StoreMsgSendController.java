package com.platform.web.controller.store;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.platform.common.annotation.Log;
import com.platform.common.enums.BusinessType;
import com.platform.store.domain.StoreMsgSend;
import com.platform.store.service.IStoreMsgSendService;
import com.platform.common.core.controller.BaseController;
import com.platform.common.core.domain.AjaxResult;
import com.platform.common.utils.poi.ExcelUtil;
import com.platform.common.core.page.TableDataInfo;

/**
 * 信息发送Controller
 * 
 * @author platform
 * @date 2020-03-04
 */
@Controller
@RequestMapping("/store/send")
public class StoreMsgSendController extends BaseController
{
    private String prefix = "store/send";

    @Autowired
    private IStoreMsgSendService storeMsgSendService;

    @RequiresPermissions("store:send:view")
    @GetMapping()
    public String send()
    {
        return prefix + "/send";
    }

    /**
     * 查询信息发送列表
     */
    @RequiresPermissions("store:send:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(StoreMsgSend storeMsgSend)
    {
        startPage();
        List<StoreMsgSend> list = storeMsgSendService.selectStoreMsgSendList(storeMsgSend);
        return getDataTable(list);
    }

    /**
     * 导出信息发送列表
     */
    @RequiresPermissions("store:send:export")
    @Log(title = "信息发送", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(StoreMsgSend storeMsgSend)
    {
        List<StoreMsgSend> list = storeMsgSendService.selectStoreMsgSendList(storeMsgSend);
        ExcelUtil<StoreMsgSend> util = new ExcelUtil<StoreMsgSend>(StoreMsgSend.class);
        return util.exportExcel(list, "send");
    }

    /**
     * 新增信息发送
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存信息发送
     */
    @RequiresPermissions("store:send:add")
    @Log(title = "信息发送", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(StoreMsgSend storeMsgSend)
    {
        return toAjax(storeMsgSendService.insertStoreMsgSend(storeMsgSend));
    }

    /**
     * 修改信息发送
     */
    @GetMapping("/edit/{msgId}")
    public String edit(@PathVariable("msgId") Long msgId, ModelMap mmap)
    {
        StoreMsgSend storeMsgSend = storeMsgSendService.selectStoreMsgSendById(msgId);
        mmap.put("storeMsgSend", storeMsgSend);
        return prefix + "/edit";
    }

    /**
     * 修改保存信息发送
     */
    @RequiresPermissions("store:send:edit")
    @Log(title = "信息发送", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(StoreMsgSend storeMsgSend)
    {
        return toAjax(storeMsgSendService.updateStoreMsgSend(storeMsgSend));
    }

    /**
     * 删除信息发送
     */
    @RequiresPermissions("store:send:remove")
    @Log(title = "信息发送", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(storeMsgSendService.deleteStoreMsgSendByIds(ids));
    }
}
