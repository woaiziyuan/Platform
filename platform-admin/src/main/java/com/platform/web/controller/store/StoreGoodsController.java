package com.platform.web.controller.store;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.platform.common.annotation.Log;
import com.platform.common.enums.BusinessType;
import com.platform.store.domain.StoreGoods;
import com.platform.store.service.IStoreGoodsService;
import com.platform.common.core.controller.BaseController;
import com.platform.common.core.domain.AjaxResult;
import com.platform.common.utils.poi.ExcelUtil;
import com.platform.common.core.page.TableDataInfo;

/**
 * 商品主Controller
 * 
 * @author platform
 * @date 2020-03-04
 */
@Controller
@RequestMapping("/store/goods")
public class StoreGoodsController extends BaseController
{
    private String prefix = "store/goods";

    @Autowired
    private IStoreGoodsService storeGoodsService;

    @RequiresPermissions("store:goods:view")
    @GetMapping()
    public String goods()
    {
        return prefix + "/goods";
    }

    /**
     * 查询商品主列表
     */
    @RequiresPermissions("store:goods:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(StoreGoods storeGoods)
    {
        startPage();
        List<StoreGoods> list = storeGoodsService.selectStoreGoodsList(storeGoods);
        return getDataTable(list);
    }

    /**
     * 导出商品主列表
     */
    @RequiresPermissions("store:goods:export")
    @Log(title = "商品主", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(StoreGoods storeGoods)
    {
        List<StoreGoods> list = storeGoodsService.selectStoreGoodsList(storeGoods);
        ExcelUtil<StoreGoods> util = new ExcelUtil<StoreGoods>(StoreGoods.class);
        return util.exportExcel(list, "goods");
    }

    /**
     * 新增商品主
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存商品主
     */
    @RequiresPermissions("store:goods:add")
    @Log(title = "商品主", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(StoreGoods storeGoods)
    {
        return toAjax(storeGoodsService.insertStoreGoods(storeGoods));
    }

    /**
     * 修改商品主
     */
    @GetMapping("/edit/{goodsId}")
    public String edit(@PathVariable("goodsId") Integer goodsId, ModelMap mmap)
    {
        StoreGoods storeGoods = storeGoodsService.selectStoreGoodsById(goodsId);
        mmap.put("storeGoods", storeGoods);
        return prefix + "/edit";
    }

    /**
     * 修改保存商品主
     */
    @RequiresPermissions("store:goods:edit")
    @Log(title = "商品主", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(StoreGoods storeGoods)
    {
        return toAjax(storeGoodsService.updateStoreGoods(storeGoods));
    }

    /**
     * 删除商品主
     */
    @RequiresPermissions("store:goods:remove")
    @Log(title = "商品主", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(storeGoodsService.deleteStoreGoodsByIds(ids));
    }
}
