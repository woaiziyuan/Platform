package com.platform.web.controller.store;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.platform.common.annotation.Log;
import com.platform.common.enums.BusinessType;
import com.platform.store.domain.StoreBlacklist;
import com.platform.store.service.IStoreBlacklistService;
import com.platform.common.core.controller.BaseController;
import com.platform.common.core.domain.AjaxResult;
import com.platform.common.utils.poi.ExcelUtil;
import com.platform.common.core.page.TableDataInfo;

/**
 * 黑名单Controller
 * 
 * @author platform
 * @date 2020-02-26
 */
@Controller
@RequestMapping("/store/blacklist")
public class StoreBlacklistController extends BaseController
{
    private String prefix = "store/blacklist";

    @Autowired
    private IStoreBlacklistService storeBlacklistService;

    @RequiresPermissions("store:blacklist:view")
    @GetMapping()
    public String blacklist()
    {
        return prefix + "/blacklist";
    }

    /**
     * 查询黑名单列表
     */
    @RequiresPermissions("store:blacklist:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(StoreBlacklist storeBlacklist)
    {
        startPage();
        List<StoreBlacklist> list = storeBlacklistService.selectStoreBlacklistList(storeBlacklist);
        return getDataTable(list);
    }

    /**
     * 导出黑名单列表
     */
    @RequiresPermissions("store:blacklist:export")
    @Log(title = "黑名单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(StoreBlacklist storeBlacklist)
    {
        List<StoreBlacklist> list = storeBlacklistService.selectStoreBlacklistList(storeBlacklist);
        ExcelUtil<StoreBlacklist> util = new ExcelUtil<StoreBlacklist>(StoreBlacklist.class);
        return util.exportExcel(list, "blacklist");
    }

    /**
     * 新增黑名单
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存黑名单
     */
    @RequiresPermissions("store:blacklist:add")
    @Log(title = "黑名单", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(StoreBlacklist storeBlacklist)
    {
        return toAjax(storeBlacklistService.insertStoreBlacklist(storeBlacklist));
    }

    /**
     * 修改黑名单
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Integer id, ModelMap mmap)
    {
        StoreBlacklist storeBlacklist = storeBlacklistService.selectStoreBlacklistById(id);
        mmap.put("storeBlacklist", storeBlacklist);
        return prefix + "/edit";
    }

    /**
     * 修改保存黑名单
     */
    @RequiresPermissions("store:blacklist:edit")
    @Log(title = "黑名单", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(StoreBlacklist storeBlacklist)
    {
        return toAjax(storeBlacklistService.updateStoreBlacklist(storeBlacklist));
    }

    /**
     * 删除黑名单
     */
    @RequiresPermissions("store:blacklist:remove")
    @Log(title = "黑名单", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(storeBlacklistService.deleteStoreBlacklistByIds(ids));
    }
}
