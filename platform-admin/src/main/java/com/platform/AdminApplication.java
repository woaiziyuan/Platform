package com.platform;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * 启动程序
 * 
 * @author platform
 */
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
public class AdminApplication
{
    public static void main(String[] args)
    {
        // System.setProperty("spring.devtools.restart.enabled", "false");
        SpringApplication.run(AdminApplication.class, args);
//        System.out.print("(♥◠‿◠)ﾉﾞ  启动成功   ლ(´ڡ`ლ)ﾞ  \n");
        System.out.print("\033[32;4m" + "(♥◠‿◠)ﾉﾞ" + "\033[0m");
        System.out.print("\033[33;4m" + "启" + "\033[0m");
        System.out.print("\033[34;4m" + "动" + "\033[0m");
        System.out.print("\033[35;4m" + "成" + "\033[0m");
        System.out.print("\033[36;4m" + "功" + "\033[0m");
        System.out.println("\033[38;4m" + " ლ(´ڡ`ლ)!" + "\033[0m");
    }
}